package com.myrewards.ceputasmania2.controller;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.myrewards.ceputasmania2.model.Menu;
import com.myrewards.ceputasmania2.model.User;
import com.myrewards.ceputasmania2.utils.DatabaseHelper;
import com.myrewards.ceputasmania2.utils.Utility;

public class BaseActivity extends Activity implements OnItemClickListener,OnClickListener{
	private final int SEARCH_BY_CATEGORIES=0;
    private final int WHAT_IS_AROUNDME=1;
    private final int HOT_OFFERS=2;
    private final int MY_FAVOURITES=3;
    private final int DAILY_DEALS=4;
    private final int EVENTS=5;
    private final int TICKETS=6;
    private final int NOTICEBOARD=7;
    private final int HELP=8;
    private final int MY_MEMBERSHIP_CARD=9;
    private final int LOGOUT=10;
    @SuppressWarnings("unused")
	private int SELECTED_MENU_ITEM;
    
    LayoutInflater inflater;
    MenuListAdapter mAdapter;
    ArrayList<Menu> menuList;
    public ListView menuListView;
    public Button menuBtn;
    int imagesList[]={R.drawable.web,
    		R.drawable.web,
    		R.drawable.web,
    		R.drawable.web,
    		R.drawable.web,
    		R.drawable.web,
    		R.drawable.web,
    		R.drawable.web,
    		R.drawable.web,
    		R.drawable.web,
    		R.drawable.web
    		};
    User user;
    
  
    public void setHeaderTitle(String title){
    	TextView titleTV = (TextView)findViewById(R.id.titleTVID);
    	titleTV.setText(title);
    }
    public void showBackButton(){
    	Button backBtn = (Button)findViewById(R.id.backToSearchBtnID);
    	backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
    	backBtn.setVisibility(View.VISIBLE);
    	backBtn.setOnClickListener(this);
    }
	public void initialiseViews(){
		menuList=new ArrayList<Menu>();
		for(int i=0;i<imagesList.length;i++){
			Menu menu=new Menu();
			menu.setImageResourceID(imagesList[i]);
			menu.setName(getResources().getStringArray(R.array.menuStrings)[i]);
			menuList.add(menu);
		}
		menuBtn=(Button)findViewById(R.id.menuBtnID);
		menuBtn.setOnClickListener(this);
		menuListView.setOnItemClickListener(this);
        mAdapter = new MenuListAdapter(this);
        menuListView.setAdapter(mAdapter);
	}
	public class MenuListAdapter extends BaseAdapter{
		
		public MenuListAdapter(BaseActivity menuListActivity) {
			
		}
		@Override
		public int getCount() {
			return menuList.size();
		}
		@Override
		public Object getItem(int arg0) {
			return null;
		}
		@Override
		public long getItemId(int arg0) {
			return 0;
		}
		@Override
		public View getView(int pos, View view, ViewGroup arg2) {
			View menuListRow = null;
			if (menuListRow == null) {
				inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				menuListRow = (ViewGroup) inflater.inflate(R.layout.menu_list_item, null, false);
			}
			TextView menuItemName = (TextView)menuListRow.findViewById(R.id.categoryTVID);
			menuItemName.setText(menuList.get(pos).getName());
			ImageView menuItemImage = (ImageView)menuListRow.findViewById(R.id.menuItemIVID);
			menuItemImage.setImageResource(menuList.get(pos).getImageResourceID());
			View line =(View)menuListRow.findViewById(R.id.lineID);
			View lineBig =(View)menuListRow.findViewById(R.id.lineBigID);
			if(pos==0)
				line.setVisibility(View.GONE);
			if(pos==9){
				lineBig.getLayoutParams().width=Utility.screenWidth;
				lineBig.getLayoutParams().height=Utility.screenWidth/24;
				lineBig.setVisibility(View.VISIBLE);
				line.setVisibility(View.GONE);
			}
			else{
				lineBig.setVisibility(View.GONE);
				line.getLayoutParams().width=5*(Utility.screenWidth/6);
			}
			return menuListRow;
		}
		
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos, long arg3) {
		if(menuListView.getVisibility() == ListView.VISIBLE){
			switch (pos) {
			case SEARCH_BY_CATEGORIES:
				SELECTED_MENU_ITEM = SEARCH_BY_CATEGORIES;
				Intent searchCategoriesIntent = new Intent(BaseActivity.this,SearchListActivity.class);
				searchCategoriesIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(searchCategoriesIntent);
				break;
			case WHAT_IS_AROUNDME:
				SELECTED_MENU_ITEM = WHAT_IS_AROUNDME;			
				Intent mapIntent = new Intent(BaseActivity.this,WhatsAroundMeActivity.class);
				mapIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(mapIntent);
				break;
			case HOT_OFFERS:
				SELECTED_MENU_ITEM = HOT_OFFERS;
				Intent hotOffersIntent = new Intent(BaseActivity.this,HotOffersActivity.class);
				hotOffersIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(hotOffersIntent);
				break;
			case MY_FAVOURITES:
				SELECTED_MENU_ITEM = MY_FAVOURITES;
				DatabaseHelper dbHelper = new DatabaseHelper(getBaseContext());
				if(dbHelper.getProductList().size() > 0){
					Intent myFavoritesIntent = new Intent(BaseActivity.this,FavoritesListActivity.class);
					startActivity(myFavoritesIntent);
				}else{
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage("No Favorites found!")
					.setNegativeButton("OK", new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
				}
				break;
			case DAILY_DEALS:
				SELECTED_MENU_ITEM = DAILY_DEALS;
				break;
			case EVENTS:
				SELECTED_MENU_ITEM = EVENTS;
				break;
			case TICKETS:
				SELECTED_MENU_ITEM = TICKETS;
				break;
			case NOTICEBOARD:
				SELECTED_MENU_ITEM = NOTICEBOARD;
				break;
			case HELP:
				SELECTED_MENU_ITEM = HELP;
				Intent membershipCardIntent = new Intent(BaseActivity.this,HelpPagesActivity.class);
				startActivity(membershipCardIntent);
				break;
			case MY_MEMBERSHIP_CARD:
				SELECTED_MENU_ITEM = MY_MEMBERSHIP_CARD;
				break;
			case LOGOUT:
				SELECTED_MENU_ITEM = LOGOUT;
				Intent logoutIntent = new Intent(BaseActivity.this,LoginScreenActivity.class);
				logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(logoutIntent);
				break;
	
			default:
				break;
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.menuBtnID:
			if(menuListView.getVisibility()==View.GONE){
				menuListView.setVisibility(View.VISIBLE);
				applyMenuListSlideAnimation(-1, 0);
			}
			else{
				applyMenuListSlideAnimation(0, -1);
				menuListView.setVisibility(View.GONE);
			}
			break;
		case R.id.backToSearchBtnID:
			Button backBtn = (Button)findViewById(R.id.backToSearchBtnID);
	    	backBtn.setVisibility(View.INVISIBLE);
			finish();
			break;
		}
		
	}
	public void applyMenuListSlideAnimation(float start, float end) {
		TranslateAnimation translate = new TranslateAnimation(
				TranslateAnimation.RELATIVE_TO_SELF, 0, 
				TranslateAnimation.RELATIVE_TO_SELF, 0, 
				TranslateAnimation.RELATIVE_TO_SELF, start, 
				TranslateAnimation.RELATIVE_TO_SELF, end);
		translate.setDuration(1000);
		translate.setStartOffset(500);
		menuListView.startAnimation(translate);
	}

}
