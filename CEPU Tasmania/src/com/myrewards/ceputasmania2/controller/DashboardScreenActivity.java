package com.myrewards.ceputasmania2.controller;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.ceputasmania2.model.NoticeId;
import com.myrewards.ceputasmania2.utils.DatabaseHelper;
import com.myrewards.ceputasmania2.utils.Utility;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.PushService;
import com.readystatesoftware.viewbadger.BadgeView;

/**
 * 
 * @author HARI This class is used to display like menu
 */

@SuppressLint("ShowToast")
public class DashboardScreenActivity extends Activity implements
		OnClickListener {

	// custom dialog fields
	public TextView logoutText;
	public Button loginbutton1;
	public Button cancelbutton1;
	final private static int LOGOUT_CEPU = 1;
	public ImageView lalertImg;
	public TextView textLog;
	ImageView noticeboardBadge;
	DatabaseHelper helper;
	static int noticeCount = 0;
	public ImageView noticeboardunread;
	public static boolean noticeboardcount = false;

	ArrayList<NoticeId> noticeid;
	DatabaseHelper dbHelper;

	/** Called when the activity is first created. */
	boolean doubleBackToExitPressedOnce = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard1);

		getWindow().setSoftInputMode(
			    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
			);
		
		dbHelper = new DatabaseHelper(this);
		try {
			// Push Notifications Adding...................
			Parse.initialize(this, "CDJtAkDyXXw04lAY6FCCrYK35y7FoLzUifX8trgT", "dmLvuJ1KKPgis4P8MCfTjZl4l7y5k5zxOOFQsABH");

			PushService.setDefaultPushCallback(this, AppPushNotificationActivity.class);
			ParseInstallation.getCurrentInstallation().saveInBackground();
			ParseAnalytics.trackAppOpened(getIntent());
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerIVID);
		headerImage.getLayoutParams().height = Utility.screenHeight / 10;

		RelativeLayout myUnionRL = (RelativeLayout) findViewById(R.id.myUnionRLID);
		myUnionRL.setOnClickListener(this);
		RelativeLayout myDelegatesRL = (RelativeLayout) findViewById(R.id.myDelegatesRLID);
		myDelegatesRL.setOnClickListener(this);
		RelativeLayout myCardRL = (RelativeLayout) findViewById(R.id.myCardRLID);
		myCardRL.setOnClickListener(this);
		RelativeLayout myUnionContactsRL = (RelativeLayout) findViewById(R.id.myUnionContactsRLID);
		myUnionContactsRL.setOnClickListener(this);
		RelativeLayout myNoticeBoardRL = (RelativeLayout) findViewById(R.id.myNoticeBoardRLID);
		myNoticeBoardRL.setOnClickListener(this);
		RelativeLayout sendAFriendRL = (RelativeLayout) findViewById(R.id.sendAFriendRLID);
		sendAFriendRL.setOnClickListener(this);
		RelativeLayout mySpecialOffersRL = (RelativeLayout) findViewById(R.id.mySpecialOffersRLID);
		mySpecialOffersRL.setOnClickListener(this);
		RelativeLayout searchMyRewardsRL = (RelativeLayout) findViewById(R.id.searchMyRewardsRLID);
		searchMyRewardsRL.setOnClickListener(this);
		// set logout from cepu
		RelativeLayout lagoutRL = (RelativeLayout) findViewById(R.id.lagoutRLID);

		lagoutRL.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog(LOGOUT_CEPU);
			}
		});

		RelativeLayout myParkingTimeRL = (RelativeLayout) findViewById(R.id.myParkingTimeRLID);
		myParkingTimeRL.setOnClickListener(this);
		RelativeLayout myissueRL = (RelativeLayout) findViewById(R.id.myissueRLID);
		myissueRL.setOnClickListener(this);
		RelativeLayout myAccountRL = (RelativeLayout) findViewById(R.id.myAccountRLID);
		myAccountRL.setOnClickListener(this);
		helper = new DatabaseHelper(DashboardScreenActivity.this);
		noticeboardunread = (ImageView) findViewById(R.id.noticeBadgeID);
		// noticeboardunread.setBackgroundResource(R.drawable.badge);
		try {
			BadgeView badge = new BadgeView(DashboardScreenActivity.this,
					noticeboardunread);
			if (helper.getNoticeDetails() != 0) {
				badge.setText(Integer.toString(helper.getNoticeDetails()));
				badge.setTextColor(Color.BLACK);
				badge.setBadgeBackgroundColor(Color.parseColor("#FFF600"));
				badge.setBadgePosition(BadgeView.POSITION_BOTTOM_LEFT);
				badge.show();
			} else {
				badge.setVisibility(View.GONE);
				noticeboardunread.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
	}

	// to create custom alerts
	protected Dialog onCreateDialog(int id) {
		try {
			AlertDialog dialogDetails = null;
			LayoutInflater inflater;
			AlertDialog.Builder dialogbuilder;
			View dialogview;
			switch (id) {
			case LOGOUT_CEPU:
				inflater = LayoutInflater.from(this);
				dialogview = inflater.inflate(R.layout.dialog_layout_logout,
						null);
				dialogbuilder = new AlertDialog.Builder(this);
				dialogbuilder.setCancelable(false);
				dialogbuilder.setView(dialogview);
				dialogDetails = dialogbuilder.create();
				break;
			case 2:
				inflater = LayoutInflater.from(this);
				dialogview = inflater.inflate(R.layout.dialog_layout_logout,
						null);
				dialogbuilder = new AlertDialog.Builder(this);
				dialogbuilder.setCancelable(false);
				dialogbuilder.setView(dialogview);
				dialogDetails = dialogbuilder.create();
				break;
			}
			return dialogDetails;
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
		return null;
	}

	protected void onPrepareDialog(int id, Dialog dialog) {
		try {
			switch (id) {
			case LOGOUT_CEPU:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				TextView alertTitleTV = (TextView) alertDialog1
						.findViewById(R.id.alertLogoutTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				logoutText = (TextView) alertDialog1
						.findViewById(R.id.my_logoutTVID);
				logoutText.setTypeface(Utility.font_reg);
				loginbutton1 = (Button) alertDialog1
						.findViewById(R.id.loginBtnH_ID);
				loginbutton1.setTypeface(Utility.font_bold);
				cancelbutton1 = (Button) alertDialog1
						.findViewById(R.id.btn_cancelH_ID);
				cancelbutton1.setTypeface(Utility.font_bold);
				alertDialog1.setCancelable(false);

				loginbutton1.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						try {
							helper.deleteLoginDetails();
							Intent logoutIntent = new Intent(
									DashboardScreenActivity.this,
									LoginScreenActivity.class);
							logoutIntent
									.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
											| Intent.FLAG_ACTIVITY_SINGLE_TOP);
							startActivity(logoutIntent);
							LoginScreenActivity.etUserId.setText("");
							LoginScreenActivity.etPasswd.setText("");
							DashboardScreenActivity.this.finish();
						} catch (Exception e) {
							if (e != null) {
								Log.w("HARI-->Debug", e);
								e.printStackTrace();
							}
						}
						alertDialog1.dismiss();
					}
				});
				cancelbutton1.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
				break;

			case 2:
				final AlertDialog alertDialog2 = (AlertDialog) dialog;
				alertTitleTV = (TextView) alertDialog2
						.findViewById(R.id.alertLogoutTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				alertTitleTV.setText("Exit !");
				logoutText = (TextView) alertDialog2
						.findViewById(R.id.my_logoutTVID);
				logoutText.setTypeface(Utility.font_reg);
				logoutText.setText("Do you want to exit the Application ?");
				loginbutton1 = (Button) alertDialog2
						.findViewById(R.id.loginBtnH_ID);
				loginbutton1.setText("YES");
				loginbutton1.setTypeface(Utility.font_bold);
				cancelbutton1 = (Button) alertDialog2
						.findViewById(R.id.btn_cancelH_ID);
				cancelbutton1.setText("NO");
				cancelbutton1.setTypeface(Utility.font_bold);
				alertDialog2.setCancelable(false);
				loginbutton1.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						finish();
					}
				});
				cancelbutton1.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
					}
				});
				break;
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.myUnionRLID:
			Intent intentMyUnion = new Intent(DashboardScreenActivity.this,	MyUnionActivity.class);
			intentMyUnion.putExtra(Utility.DASHBOARD_ICON_ID, Utility.MY_UNION);
			startActivity(intentMyUnion);
			break;
		case R.id.myDelegatesRLID:
			Intent intentMyDelegates = new Intent(DashboardScreenActivity.this,	MyDelegatesActivity.class);
			intentMyDelegates.putExtra(Utility.DASHBOARD_ICON_ID, Utility.MY_DELEGATES);
			startActivity(intentMyDelegates);
			break;
		case R.id.myCardRLID:
			if (isNetworkAvailable()) {
				Intent intentMyCard = new Intent(DashboardScreenActivity.this, MyCardActivity.class);
				intentMyCard.putExtra(Utility.DASHBOARD_ICON_ID, Utility.MY_CARD);
				startActivity(intentMyCard);
			} else {
				showHariCustomToast();
			}
			break;
		case R.id.myUnionContactsRLID:
			Intent intentMyUnionContacts = new Intent(DashboardScreenActivity.this, MyUnionContactsActivity.class);
			intentMyUnionContacts.putExtra(Utility.DASHBOARD_ICON_ID, Utility.MY_UNION_CONTACTS);
			startActivity(intentMyUnionContacts);
			break;
		case R.id.myNoticeBoardRLID:
			if (isNetworkAvailable()) {
				Intent intentMyNoticeBoard = new Intent(DashboardScreenActivity.this, MyNoticeBoardActivity.class);
				intentMyNoticeBoard.putExtra(Utility.DASHBOARD_ICON_ID,	Utility.MY_NOTICE_BOARD);
				startActivity(intentMyNoticeBoard);
				DashboardScreenActivity.this.finish();
			} else {
				showHariCustomToast();
			}
			break;
		case R.id.sendAFriendRLID:
			Intent intentSendaFriend = new Intent(DashboardScreenActivity.this, SendAFriendNewActivity.class);
			intentSendaFriend.putExtra(Utility.DASHBOARD_ICON_ID, Utility.SEND_A_FRIEND);
			startActivity(intentSendaFriend);
			break;
		case R.id.mySpecialOffersRLID:
			if (isNetworkAvailable()) {
				Intent intentGIN1 = new Intent(DashboardScreenActivity.this, GrabitNowTabsActivity.class);
				intentGIN1.putExtra(Utility.DASHBOARD_ICON_ID, 1);
				startActivity(intentGIN1);
			} else {
				showHariCustomToast();
			}
			break;
		case R.id.searchMyRewardsRLID:
			if (isNetworkAvailable()) {
				Intent intentGIN2 = new Intent(DashboardScreenActivity.this, GrabitNowTabsActivity.class);
				intentGIN2.putExtra(Utility.DASHBOARD_ICON_ID, 2);
				startActivity(intentGIN2);
			} else {
				showHariCustomToast();
			}
			break;
		case R.id.myParkingTimeRLID:
			Intent intentMyParking = new Intent(DashboardScreenActivity.this, NewParkingTimeActivity.class);
			intentMyParking.putExtra(Utility.DASHBOARD_ICON_ID, Utility.MY_PARKING_TIME);
			startActivity(intentMyParking);
			break;
		case R.id.myissueRLID:
			Intent intentMyIssue = new Intent(DashboardScreenActivity.this, MyIssueActivity.class);
			intentMyIssue.putExtra(Utility.DASHBOARD_ICON_ID, Utility.MY_ISSUE);
			startActivity(intentMyIssue);
			break;
		case R.id.myAccountRLID:
			if (isNetworkAvailable()) {
				Intent intentMyAccount = new Intent(DashboardScreenActivity.this, MyAccountActivity.class);
				intentMyAccount.putExtra(Utility.DASHBOARD_ICON_ID,Utility.MY_ACCOUNT);
				startActivity(intentMyAccount);
			} else {
				showHariCustomToast();
			}
			break;
		}
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			return false;
		} else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private void showHariCustomToast() {
		try {
			LayoutInflater inflater = Utility.from_HariContext(DashboardScreenActivity.this);
			View layout = null;
			if (inflater != null) {
				layout = inflater.inflate(R.layout.toast_no_netowrk, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				if (layout != null) {
					toast.setView(layout);
					toast.show();
				}
			} else {
				Toast.makeText(getApplicationContext(), "No Netwrok Connection!!!", 2500).show();
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
			}
			Toast.makeText(getApplicationContext(), "No Netwrok Connection!!!",	2500).show();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			exitApplication();
			// showDialog(2);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	private void exitApplication() {

		if (doubleBackToExitPressedOnce) {

			DashboardScreenActivity.this.finish();

		} else {
			doubleBackToExitPressedOnce = true;
			Toast.makeText(DashboardScreenActivity.this,
					"Press again to minimize the app.", 100).show();
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					doubleBackToExitPressedOnce = false;
				}
			}, 2000);

		}

	}
}