package com.myrewards.ceputasmania2.controller;

import java.util.List;
import java.util.Vector;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.myrewards.ceputasmania2.model.HelpPageFive;
import com.myrewards.ceputasmania2.model.HelpPageFour;
import com.myrewards.ceputasmania2.model.HelpPageOne;
import com.myrewards.ceputasmania2.model.HelpPageSix;
import com.myrewards.ceputasmania2.model.HelpPageThree;
import com.myrewards.ceputasmania2.model.HelpPageTwo;


/**
 * The <code>ViewPagerFragmentActivity</code> class is the fragment activity hosting the ViewPager  
 * @author mwho
 */
public class HelpPagesActivity extends FragmentActivity{
	/** maintains the pager adapter*/
	private PagerAdapter mPagerAdapter;
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.helpviewpager_layout);
		try {
			//initialsie the pager
			this.initialisePaging();
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Initialise the fragments to be paged
	 */
	private void initialisePaging() {
		List<Fragment> fragments = new Vector<Fragment>();
		fragments.add(Fragment.instantiate(this, HelpPageOne.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageTwo.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageThree.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageFour.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageFive.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageSix.class.getName()));
		this.mPagerAdapter  = new PagerAdapter(super.getSupportFragmentManager(), fragments);
		ViewPager pager = (ViewPager)super.findViewById(R.id.viewpager);
		pager.setAdapter(this.mPagerAdapter);
	}
}
