package com.myrewards.ceputasmania2.controller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.ceputasmania2.model.User;
import com.myrewards.ceputasmania2.service.CEPUService;
import com.myrewards.ceputasmania2.service.CEPUServiceListener;
import com.myrewards.ceputasmania2.utils.Utility;

@SuppressLint("ShowToast")
public class MyAccountActivity extends Activity implements CEPUServiceListener,
		OnClickListener, OnItemSelectedListener, OnLongClickListener {
	EditText firstnameET, lastnameET, emailET;// membershipnumberET
	EditText stateET, countryET;
	private Spinner stateSP2, countrySP1;
	View loading;
	Button backButton, scanBarBtn,submitBtn;
	TextView titleTV;
	private final static int UPDATE_ACCOUNT_SUCCUSS = 1;
	private final static int UPDATE_ACCOUNT_All_FIELDS = 2;
	private final static int NO_NETWORK_CON = 3;
	private final static int MY_ACCOUNT_STRINGS = 4;
	private static final int DIALOG_SEND_FAILED = 5;
	final private static int DIALOG_SEND_INVALID_EMAIL = 6;
	TextView tv11, tv12, textTV, textTV2;
	TextView alertMsgTV, alertTitleTV;
	Button okbutton;
	private TextView fNameTV, lNameTV, emailTV, countryTV, stateTV;

	// shambhi logic here for spinner

	int[] spinnerRIDS = { R.array.category_state_australia,
			R.array.category_state_hongkong, R.array.category_state_india,
			R.array.category_state_newzealand,
			R.array.category_state_philippines,
			R.array.category_state_singapore };
	String countries[] = { "Australia", "Hong Kong", "India", "New Zealand",
			"Philippines", "Singapore" };

	String stateSPITEMS[][] = {
			{ "", "ACT", "NSW", "NT", "QLD", "SA", "TAS", "VIC", "WA" },
			{ "HK" },
			{ "", "AP", "DL", "GJ", "HR", "KA", "MH", "TN", "UP", "WB" },
			{ "", "AUK", "BOP", "CAN", "FIL", "GIS", "HKB", "MBH", "MWT",
					"NAB", "NTL", "OTA", "STL", "TKI", "TMR", "TSM", "WGI",
					"WGN", "WKO", "WPP", "WTC" },
			{ "", "LUZ", "MIN", "NCR", "VIS" },
			{ "", "CS", "NES", "NWS", "SES", "SWS" } };

	String countrySPITEMS[] = { "", "AU", "HK", "IN", "NZ", "PH", "SG" };

	String cCode = "", stateName = null;
	ArrayAdapter<CharSequence> adapter2 = null;
	String subDomainURLString = "www.myrewards.com.au";

	String stateCode,mCountrySP,state ;
	
	// ProgressDialog myPd_ring;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_account);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5f);

		titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText(getResources().getString(R.string.my_account));

		fNameTV = (TextView) findViewById(R.id.firstNameAccountTVID);
		fNameTV.setTypeface(Utility.font_reg);
		lNameTV = (TextView) findViewById(R.id.lastNameAccountTVID);
		lNameTV.setTypeface(Utility.font_reg);
		emailTV = (TextView) findViewById(R.id.emailAccountTVID);
		emailTV.setTypeface(Utility.font_reg);
		countryTV = (TextView) findViewById(R.id.stateAccountTVID);
		countryTV.setTypeface(Utility.font_reg);
		stateTV = (TextView) findViewById(R.id.stateAccountdTVID);
		stateTV.setTypeface(Utility.font_reg);

		scanBarBtn = (Button) findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);

		loading = (View) findViewById(R.id.loading);
		 submitBtn = (Button) findViewById(R.id.submitBtnID);
		submitBtn.getLayoutParams().height = (int) (Utility.screenHeight / 16.0f);
		submitBtn.setOnClickListener(this);
		submitBtn.setWidth(Utility.screenWidth / 2);
		ImageView logoIV = (ImageView) findViewById(R.id.logoIVID);
		logoIV.getLayoutParams().width = (int) (Utility.screenWidth / 3.5f);
		logoIV.getLayoutParams().height = (int) (Utility.screenWidth / 3.5f);

		firstnameET = (EditText) findViewById(R.id.firstnameETID);
		firstnameET.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		firstnameET.setTypeface(Utility.font_reg);

		lastnameET = (EditText) findViewById(R.id.lastnameETID);
		lastnameET.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		lastnameET.setTypeface(Utility.font_reg);

		firstnameET.setOnLongClickListener(this);
		lastnameET.setOnLongClickListener(this);

		emailET = (EditText) findViewById(R.id.emailETID);
		emailET.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		emailET.setTypeface(Utility.font_reg);
		emailET.setOnLongClickListener(this);

		stateET = (EditText) findViewById(R.id.stateETID);
		stateET.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		stateET.setTypeface(Utility.font_reg);
		stateET.setOnLongClickListener(this);
		stateET.setOnClickListener(this);
		countryET = (EditText) findViewById(R.id.countryETID);
		countryET.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		countryET.setTypeface(Utility.font_reg);
		countryET.setOnLongClickListener(this);
		countryET.setOnClickListener(this);

		// back to dash board screen
		backButton = (Button) findViewById(R.id.backBtnID);
		backButton.getLayoutParams().width = (int) (Utility.screenWidth / 8.5f);
		backButton.getLayoutParams().height = (int) (Utility.screenHeight / 20.0f);
		backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		// select country
		countrySP1 = (Spinner) findViewById(R.id.countrySpinnerID);
		countrySP1.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		countrySP1.setOnItemSelectedListener(this);

		// select state
		stateSP2 = (Spinner) findViewById(R.id.stateSpinnerID);
		stateSP2.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		stateSP2.setOnItemSelectedListener(this);

		submitBtn.setEnabled(false);
		scanBarBtn.setEnabled(false);
		firstnameET.setEnabled(false);
		emailET.setEnabled(false);
		stateET.setEnabled(false);
		countryET.setEnabled(false);
		backButton.setEnabled(false);
		countrySP1.setEnabled(false);
		stateSP2.setEnabled(false);
		
		stateSP2.setVisibility(View.GONE);
		ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(
				this, R.array.category_countries,
				android.R.layout.simple_spinner_item);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		countrySP1.setAdapter(adapter1);

		countrySP1.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				ArrayAdapter<CharSequence> adapter2 = null;
				if (arg2 == 0) {
					adapter2 = ArrayAdapter.createFromResource(
							MyAccountActivity.this, R.array.selectstatearray,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);

					}
				} else if (arg2 == 1) {
					adapter2 = ArrayAdapter.createFromResource(
							MyAccountActivity.this,
							R.array.category_state_australia,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);

					}

				} else if (arg2 == 2) {
					adapter2 = ArrayAdapter.createFromResource(
							MyAccountActivity.this,
							R.array.category_state_hongkong,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				} else if (arg2 == 3) {
					adapter2 = ArrayAdapter.createFromResource(
							MyAccountActivity.this,
							R.array.category_state_india,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);

					}
				} else if (arg2 == 4) {
					adapter2 = ArrayAdapter.createFromResource(
							MyAccountActivity.this,
							R.array.category_state_newzealand,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);

					}
				} else if (arg2 == 5) {
					adapter2 = ArrayAdapter.createFromResource(
							MyAccountActivity.this,
							R.array.category_state_philippines,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				} else if (arg2 == 6) {
					adapter2 = ArrayAdapter.createFromResource(
							MyAccountActivity.this,
							R.array.category_state_singapore,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		if (Utility
				.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			CEPUService.getCEPUService().sendLoginRequest(this,
					Utility.userName, Utility.pwd, subDomainURLString);
		} else {
			loading.setVisibility(View.GONE);
			// The Custom Toast Layout Imported here
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));

			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();
			MyAccountActivity.this.finish();
		}
		loadEditBoxes();
	}

	private void loadEditBoxes() {
		try {
			Resources res = getResources();
			String[] str = null;
			if (Utility.user.getFirst_name() != null)
				firstnameET.setText(Utility.user.getFirst_name());
			
			if (Utility.user.getLast_name() != null)
				lastnameET.setText(Utility.user.getLast_name());
			
			if (Utility.user.getEmail() != null)
				emailET.setText(Utility.user.getEmail());
			
			for (int i = 0; i < countries.length; i++) {
				if (countries[i].equals(Utility.user.getCountry())) {
					str = res.getStringArray(spinnerRIDS[i]);

					for (int j = 0; j < str.length; j++) {
						if (stateSPITEMS[i][j].equals(Utility.user.getState())) {
							stateName = str[j];
							break;
						}
					}
				}

			}
			
			if (Utility.user.getState() != null) {
				stateET.setText(stateName);
			} else {
				stateET.setText("Select State");
			}
			if (Utility.user.getCountry() != null) {
				countryET.setText(Utility.user.getCountry());
			} else {
				countryET.setText("Select Country");
			}
			
			/*if (stateName != null)
				stateET.setText(stateName);
			else {
				stateET.setText("Select State");
			}
			if (Utility.user.getCountry() != null)
				countryET.setText(Utility.user.getCountry());
			else {
				countryET.setText("Select Country");
			}*/
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onServiceComplete(Object response, int eventType) {
		loading.setVisibility(View.GONE);
		
		submitBtn.setEnabled(true);
		scanBarBtn.setEnabled(true);
		firstnameET.setEnabled(true);
		emailET.setEnabled(true);
		stateET.setEnabled(true);
		countryET.setEnabled(true);
		backButton.setEnabled(true);
		countrySP1.setEnabled(true);
		stateSP2.setEnabled(true);
		
		// myPd_ring.dismiss();
		try {
			if (response != null && eventType == 9) {

				if (response instanceof String) {
					if (response.toString().contains("success")) {

						Utility.user.setFirst_name(firstnameET.getText()
								.toString());
						Utility.user.setLast_name(lastnameET.getText()
								.toString());
						Utility.user.setEmail(emailET.getText().toString());
						Utility.user.setCountry(mCountrySP);
						Utility.user.setState(stateCode);
						
						showDialog(UPDATE_ACCOUNT_SUCCUSS);
					} else
						showDialog(DIALOG_SEND_FAILED);
				}
			}

			else if (eventType != 14 && eventType != 16) {

				if (response != null) {
					if (response instanceof String) {

						// Utility.showMessage(this, response.toString());
					} else {

						Utility.user = (User) response;

						loadEditBoxes();
						// loading.setVisibility(View.GONE);

					}
				}

			}
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	boolean isEmailValid(CharSequence email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.submitBtnID) {
			try {
				final String mEmail = emailET.getText().toString();
				String fName = firstnameET.getText().toString();
				String lName = lastnameET.getText().toString();

				if (stateSP2.getSelectedItem() != null
						&& countrySP1.getSelectedItem() != null) {
					 state = stateSP2.getSelectedItem().toString();
					 mCountrySP = countrySP1.getSelectedItem().toString();

					int countrySPPOS = countrySP1.getSelectedItemPosition();
					int stateSPPOS = stateSP2.getSelectedItemPosition();

					if (fName.length() < 1 || lName.length() < 1
							|| state.equals("Select State")
							|| mCountrySP.equals("Select Country")
							|| mEmail.length() < 1) {
						showDialog(UPDATE_ACCOUNT_All_FIELDS);
					} else {
						if (Utility
								.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							if (isEmailValid(mEmail)) {
								loading.setVisibility(View.VISIBLE);
								
								stateCode=stateSPITEMS[countrySPPOS - 1][stateSPPOS];
								
								CEPUService.getCEPUService().sendMyAccountRequest(
										this,
										Utility.user.getId(),
										firstnameET.getText()
												.toString(),
										lastnameET.getText().toString(),
										mEmail,
										mCountrySP,
										stateCode,
										Utility.user.getNewsletter());
							} else {
								showDialog(DIALOG_SEND_INVALID_EMAIL);
							}
						} else {
							showDialog(NO_NETWORK_CON);
						}
					}
				} else {

					 stateCode = getStateCode(stateET.getText()
								.toString(), countryET.getText().toString());

						 state = stateET.getText().toString();

						mCountrySP = countryET.getText().toString();

					if (fName.length() < 1 || lName.length() < 1

					|| mEmail.length() < 1 || state.equals("Select State")
							|| mCountrySP.equals("Select Country")) {
						showDialog(UPDATE_ACCOUNT_All_FIELDS);
					} else {

						stateET.setText(stateET.getText().toString());
						countryET.setText(countryET.getText().toString());

						if (Utility
								.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							if (isEmailValid(mEmail)) {
								loading.setVisibility(View.VISIBLE);
								CEPUService
										.getCEPUService()
										.sendMyAccountRequest(
												this,
												Utility.user.getId(),
												firstnameET.getText()
														.toString(),
												lastnameET.getText().toString(),
												mEmail, mCountrySP, stateCode,
												Utility.user.getNewsletter());
								// myPd_ring =
								// ProgressDialog.show(MyAccountActivity.this,
								// "Please wait", "Loading please wait..",
								// true);
							} else {
								showDialog(DIALOG_SEND_INVALID_EMAIL);
								// Toast.makeText(this, "HariKrishna",
								// 3000).show();c
							}
						} else {
							showDialog(NO_NETWORK_CON);
							// showDialog("My Account",
							// "No network connection available",
							// 2);
						}
					}

				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("HARI-->Debug", e);
					e.printStackTrace();
				}
			}
		}

		else if (v.getId() == R.id.countryETID) {
			stateET.setVisibility(View.GONE);
			countryET.setVisibility(View.GONE);

			stateSP2.setVisibility(View.VISIBLE);
			countrySP1.setVisibility(View.VISIBLE);
			countrySP1.performClick();
		} else if (v.getId() == R.id.stateETID) {

			if (countryET.getVisibility() == View.VISIBLE) {
				Toast.makeText(this, "select country first", 2000).show();
			} else {
				stateET.setVisibility(View.GONE);
				stateSP2.setVisibility(View.VISIBLE);
				stateSP2.performClick();
			}
		}
	}

	@SuppressWarnings("unused")
	private String getStateCode(String st, String country) {

		String state = null;
		int a = 0, b = 0;
		Resources res = getResources();
		String[] str = null;

		for (int i = 0; i < countries.length; i++) {

			if (countries[i].equals(country)) {
				str = res.getStringArray(spinnerRIDS[i]);
				a = i;
				break;

			}
		}

		for (int j = 0; j < str.length; j++) {
			if (str[j].equals(st)) {
				state = stateSPITEMS[a][j];
				break;
			}
		}

		if (state.equals("")) {
			state = "Select State";
		}

		return state;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == 1) {
			AlertDialog fieldsDialog = null;
			switch (id) {
			case UPDATE_ACCOUNT_SUCCUSS:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.dailog_layout_my_account_success, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setCancelable(false);
				allFalert.setView(allFieldsView);
				fieldsDialog = allFalert.create();
				break;
			}
			return fieldsDialog;
		} else if (id == 2) {
			AlertDialog fieldsDialog = null;
			switch (id) {
			case UPDATE_ACCOUNT_All_FIELDS:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.dailog_layout_update_all_man_fields, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setCancelable(false);
				allFalert.setView(allFieldsView);
				fieldsDialog = allFalert.create();
				break;
			}
			return fieldsDialog;
		} else if (id == 3) {
			AlertDialog noNetworkDialog = null;
			switch (id) {
			case NO_NETWORK_CON:
				LayoutInflater noNetInflater = LayoutInflater.from(this);
				View noNetworkView = noNetInflater.inflate(
						R.layout.dialog_layout_no_network, null);
				AlertDialog.Builder adbNoNet = new AlertDialog.Builder(this);
				adbNoNet.setCancelable(false);
				adbNoNet.setView(noNetworkView);
				noNetworkDialog = adbNoNet.create();
				break;
			}
			return noNetworkDialog;
		} else if (id == 4) {
			AlertDialog noNetworkDialog = null;
			switch (id) {
			case MY_ACCOUNT_STRINGS:
				LayoutInflater noNetInflater = LayoutInflater.from(this);
				View noNetworkView = noNetInflater.inflate(
						R.layout.dailkog_layout_my_account_strings, null);
				AlertDialog.Builder adbNoNet = new AlertDialog.Builder(this);
				adbNoNet.setCancelable(false);
				adbNoNet.setView(noNetworkView);
				noNetworkDialog = adbNoNet.create();
				break;
			}
			return noNetworkDialog;
		} else if (id == 5) {
			AlertDialog dialogDetails2 = null;
			switch (id) {
			case DIALOG_SEND_FAILED:
				LayoutInflater inflater2 = LayoutInflater.from(this);
				View dialogview = inflater2.inflate(
						R.layout.dialog_send_a_frnd_failed, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(
						this);
				dialogbuilder.setCancelable(false);
				dialogbuilder.setView(dialogview);
				dialogDetails2 = dialogbuilder.create();
				break;
			}
			return dialogDetails2;
		} else if (id == 6) {
			AlertDialog fieldsDialog = null;
			switch (id) {
			case DIALOG_SEND_INVALID_EMAIL:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.dialog_send_a_frnd_invalid, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setCancelable(false);
				allFalert.setView(allFieldsView);
				fieldsDialog = allFalert.create();
				break;
			}
			return fieldsDialog;
		}
		return super.onCreateDialog(id);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		if (id == 1) {
			switch (id) {
			case UPDATE_ACCOUNT_SUCCUSS:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				alertTitleTV = (TextView) alertDialog1
						.findViewById(R.id.alertLogoutTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				alertMsgTV = (TextView) alertDialog1
						.findViewById(R.id.updateAccountTVID);
				alertMsgTV.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog1
						.findViewById(R.id.updateAccountOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				alertDialog1.setCancelable(false);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						MyAccountActivity.this.finish();
						alertDialog1.dismiss();
					}
				});
				break;
			}
		} else if (id == 2) {
			switch (id) {
			case UPDATE_ACCOUNT_All_FIELDS:
				final AlertDialog alertDialog2 = (AlertDialog) dialog;
				alertTitleTV = (TextView) alertDialog2
						.findViewById(R.id.firstLoginCEPUTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				alertMsgTV = (TextView) alertDialog2
						.findViewById(R.id.manFieldAccountTVID);
				alertMsgTV.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog2
						.findViewById(R.id.manFieldAccountOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				alertDialog2.setCancelable(false);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
					}
				});
				break;
			}
		} else if (id == 3) {
			switch (id) {
			case NO_NETWORK_CON:
				final AlertDialog alertDialog3 = (AlertDialog) dialog;
				alertTitleTV = (TextView) alertDialog3
						.findViewById(R.id.alertLogoutTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				tv12 = (TextView) alertDialog3.findViewById(R.id.noConnTVID);
				tv12.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog3
						.findViewById(R.id.noNetWorkOKID);
				okbutton.setTypeface(Utility.font_bold);
				alertDialog3.setCancelable(false);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog3.dismiss();
					}
				});
				break;
			}
		} else if (id == 4) {
			switch (id) {
			case MY_ACCOUNT_STRINGS:
				final AlertDialog alertDialog4 = (AlertDialog) dialog;
				alertTitleTV = (TextView) alertDialog4
						.findViewById(R.id.alertLogoutTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				tv12 = (TextView) alertDialog4
						.findViewById(R.id.myAccInvalidTVID);
				tv12.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog4
						.findViewById(R.id.myAccinValidOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				alertDialog4.setCancelable(false);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						MyAccountActivity.this.finish();
						alertDialog4.dismiss();
					}
				});
				break;
			}
		} else if (id == 5) {
			final AlertDialog alertDialog2 = (AlertDialog) dialog;
			alertTitleTV = (TextView) alertDialog2
					.findViewById(R.id.alertSucTitleTVID);
			alertTitleTV.setTypeface(Utility.font_bold);
			textTV2 = (TextView) alertDialog2
					.findViewById(R.id.alertSucTitleTVID);
			textTV2.setTypeface(Utility.font_reg);
			textTV2.setText("Fail !");
			textTV = (TextView) alertDialog2.findViewById(R.id.failedSentTVID);
			textTV.setTypeface(Utility.font_reg);
			textTV.setText("Update failed.");
			okbutton = (Button) alertDialog2
					.findViewById(R.id.sendFailedOkBtnID);
			okbutton.setTypeface(Utility.font_bold);
			alertDialog2.setCancelable(false);
			okbutton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialog2.dismiss();
				}
			});
		} else if (id == 6) {
			final AlertDialog alertDialog1 = (AlertDialog) dialog;
			alertTitleTV = (TextView) alertDialog1
					.findViewById(R.id.firstLoginCEPUErrorTitleTVID);
			alertTitleTV.setTypeface(Utility.font_bold);
			textTV = (TextView) alertDialog1
					.findViewById(R.id.inValidFieldTVID);
			textTV.setTypeface(Utility.font_reg);
			okbutton = (Button) alertDialog1.findViewById(R.id.inValidOKBtnID);
			okbutton.setTypeface(Utility.font_bold);
			alertDialog1.setCancelable(false);
			okbutton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialog1.dismiss();
				}
			});
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onLongClick(View v) {
		boolean returnValue = true;
		if (v.getId() == R.id.firstnameETID) {
			if (firstnameET.getText().length() > 25) {
				returnValue = true;

			} else {
				returnValue = false;
			}
		} else if (v.getId() == R.id.lastnameETID) {
			if (emailET.getText().length() > 25) {
				returnValue = true;

			} else {
				returnValue = false;
			}
		} else if (v.getId() == R.id.emailETID) {
			if (lastnameET.getText().length() > 25) {
				returnValue = true;

			} else {
				returnValue = false;
			}
		}
		return returnValue;
	}
}
