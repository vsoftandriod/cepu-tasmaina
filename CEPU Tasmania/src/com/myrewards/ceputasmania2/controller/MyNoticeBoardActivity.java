package com.myrewards.ceputasmania2.controller;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.myrewards.ceputasmania2.model.NoticeBoard;
import com.myrewards.ceputasmania2.model.NoticeId;
import com.myrewards.ceputasmania2.model.ProductNotice;
import com.myrewards.ceputasmania2.service.CEPUService;
import com.myrewards.ceputasmania2.service.CEPUServiceListener;
import com.myrewards.ceputasmania2.utils.DatabaseHelper;
import com.myrewards.ceputasmania2.utils.Utility;

public class MyNoticeBoardActivity extends Activity implements CEPUServiceListener,
		OnItemClickListener, OnClickListener {

	List<ProductNotice> productsList;
	ProductNotice productNotice;
	List<NoticeBoard> noticeBoardProductsList;
	LayoutInflater inflater;
	public static boolean noticecount = false;
	ListView noticeboardListView;
	NoticeboardAdapter mAdapter;
	DatabaseHelper helper;
	View loading;
	Button backBtn, scanBarBtn;
	TextView titleTV;
	public static int count1 = 0, count2 = 0, count3 = 0;
	ImageView noticeArrow;
	TextView productNameTV, createdTitleTV;
	ImageView noticeImage;
	
	// show dialog...........
	final private static int NO_NETWORK_CON = 1;
	public TextView tv12;
	public Button okbutton;

	ArrayList<NoticeId> noticeid;
	
	ToggleButton tButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_notice_board);
		
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		
		titleTV=(TextView)findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText(getResources().getString(R.string.notice_board_text));
		
		scanBarBtn=(Button)findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);	
		
		loading = (View) findViewById(R.id.loading);
		helper = new DatabaseHelper(this);
		backBtn = (Button) findViewById(R.id.backBtnID);
		backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		
		RelativeLayout noticeUpdateRL=(RelativeLayout)findViewById(R.id.noticeUpdateRLID);
		noticeUpdateRL.setVisibility(View.VISIBLE);
		
		TextView noticeUpdateText=(TextView)findViewById(R.id.noticeUpdateTVID);
		noticeUpdateText.setTypeface(Utility.font_bold);
		
		tButton = (ToggleButton) findViewById(R.id.toggleButton1);
		
		tButton.getLayoutParams().width = (int) (Utility.screenWidth / 7);
		tButton.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		if(helper.getNoticeUpdateState().equals("ON"))
		tButton.setChecked(true);
		else
			tButton.setChecked(false);
		tButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				try {
					if(isChecked)
					{
						helper.setNoticeUpdateState("ON");
						Utility.setNotificationReceiver(MyNoticeBoardActivity.this);
					}
					else
					{
						helper.setNoticeUpdateState("OFF");
						Utility.cancelNotificationReceiver();
					}
				} catch (Exception e) {
					if (e != null) {
						Log.w("HARI-->Debug", e);
						e.printStackTrace();
					}
				}
			}
		});
		
		backBtn.setOnClickListener(this);

		if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			loading.setVisibility(View.VISIBLE);
			CEPUService.getCEPUService().sendMyNoticeBoardRequest(this);
		} else {
			showDialog(NO_NETWORK_CON);
		}
		noticeboardListView = (ListView) findViewById(R.id.noticeboardListViewID);
		noticeboardListView.setOnItemClickListener(this);

	}

	public class NoticeboardAdapter extends BaseAdapter {

		public NoticeboardAdapter(MyNoticeBoardActivity myNoticeBoardActivity) {

		}

		@Override
		public int getCount() {
			return productsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {

			View row = null;
			if (row == null) {
				inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = (View) inflater.inflate(R.layout.noticeboard_list_item, null, false);
			}
			productNameTV = (TextView) row.findViewById(R.id.productNoticeTVID);
			productNameTV.setSelected(true);
			RelativeLayout rowLL2 = (RelativeLayout)row.findViewById(R.id.resultItemNotice5RLID);
			rowLL2.getLayoutParams().height = (int) (Utility.screenHeight / 9.4);
			
			
			productNameTV.setTypeface(Utility.font_bold);
			//createdTitleTV = (TextView) row.findViewById(R.id.createdTitleTVID);
			//createdTitleTV.setText(productNotice.getCreated());
			noticeImage = (ImageView) row.findViewById(R.id.imageView1);
			noticeArrow = (ImageView) row.findViewById(R.id.arrowIVID);
			productNameTV.setText(productsList.get(pos).getSubject());
			RelativeLayout rowRL = (RelativeLayout) row.findViewById(R.id.resultItemNoticeRLID);
			switch (pos % 4) {
			case 0:
				rowRL.setBackgroundResource(R.color.result_color_one_main);
				productNameTV.setTextColor(Color.parseColor("#2E52AC"));
				break;
			case 1:
				rowRL.setBackgroundResource(R.color.result_color_two_main);
				productNameTV.setTextColor(Color.parseColor("#60B3FF"));
				break;
			case 2:
				rowRL.setBackgroundResource(R.color.result_color_three_main);
				productNameTV.setTextColor(Color.parseColor("#2E52AC"));
				break;
			case 3:
				rowRL.setBackgroundResource(R.color.result_color_four_main);
				productNameTV.setTextColor(Color.parseColor("#60B3FF"));
				break;
			}
			if (helper.getNoticeIdReadorUnread(productsList.get(pos).getId()).equals("CLOSE")) {
				noticeImage.setImageResource(R.drawable.notice_read);
			} else if(helper.getNoticeIdReadorUnread(productsList.get(pos).getId()).equals("OPEN")) {
				noticeImage.setImageResource(R.drawable.notice_unread);
			}
			return row;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		if (response != null) {
			if(eventType!=16)
			{
			try {
				if (response instanceof String) {
				} else {

					productsList = (List<ProductNotice>) response;
					mAdapter = new NoticeboardAdapter(this);
					noticeboardListView.setAdapter(mAdapter);
				}
				noticeBoardUpdates();
			} catch (Exception e) 
			{
				e.printStackTrace();
			}
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
		noticecount = true;
		try {
			helper = new DatabaseHelper(this);
			helper.updateNoticeDetails(productsList.get(pos).getId());

			ProductNotice tappedItem = productsList.get(pos);
			Intent intent = new Intent(this, MyNoticeBoardDetailsActivity.class);
			intent.putExtra(MyNoticeBoardDetailsActivity.DETAILS, tappedItem);
			startActivity(intent);
			MyNoticeBoardActivity.this.finish();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	// custom dialog..........
	@Override
	protected Dialog onCreateDialog(int id) {
		if(id==1)
		{
			AlertDialog noNetworkDialog=null;
			switch (id) {
			case NO_NETWORK_CON:
				LayoutInflater noNetInflater=LayoutInflater.from(this);
				View noNetworkView=noNetInflater.inflate(R.layout.dialog_layout_no_network, null);
				AlertDialog.Builder adbNoNet=new AlertDialog.Builder(this);	
				adbNoNet.setCancelable(false);
				adbNoNet.setView(noNetworkView);
				noNetworkDialog=adbNoNet.create();
				break;
			}
			return noNetworkDialog;
		}
	       	return null;
	}
	
	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		if(id==1){
			switch (id) {
			case NO_NETWORK_CON:
				final AlertDialog alertDialog2 = (AlertDialog) dialog;
				TextView alertTitle=(TextView)alertDialog2.findViewById(R.id.alertLogoutTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);
				tv12=(TextView)alertDialog2.findViewById(R.id.noConnTVID);
				tv12.setTypeface(Utility.font_reg);
				okbutton= (Button) alertDialog2.findViewById(R.id.noNetWorkOKID);
				okbutton.setTypeface(Utility.font_bold);
				alertDialog2.setCancelable(false);
				okbutton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				

				alertDialog2.dismiss();
				startActivity(new Intent(MyNoticeBoardActivity.this,DashboardScreenActivity.class));
				finish();
			
					}
				});
				break;
			}
		}
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.backBtnID:
			try {
				noticeBoardUpdates();
				startActivity(new Intent(this, DashboardScreenActivity.class));
				MyNoticeBoardActivity.this.finish();
			} catch (Exception e) {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				MyNoticeBoardActivity.this.finish();
			}
			break;
		}
	}
	
	private void noticeBoardUpdates() {
		try {
			noticeid = new ArrayList<NoticeId>();
			for(int i=0;i<productsList.size();i++)
			{
				noticeid.add(new NoticeId(productsList.get(i).getId()));
			}
			if (noticeid != null)
			{
				List<String> shambhiList=new ArrayList<String>();
				for(int n=0;n<noticeid.size();n++)
				{
					if(!(noticeid.get(n).getNoticeDetails()==null))
						shambhiList.add(noticeid.get(n).getNoticeDetails());
				}
					if(shambhiList.size()!=helper.getExistingIDs().size())
					{
						for(int m=0;m<helper.getExistingIDs().size();m++)
						{
							if(shambhiList.contains(helper.getExistingNoticeRealIDs().get(m)))
							{
								
							}
							else
							{
								helper.deleteNoticeIdDetails(helper.getExistingNoticeRealIDs().get(m));
								
							}
						}
					}
				
				for (int k = 0; k < noticeid.size(); k++) {
					if (noticeid.get(k).getNoticeDetails() != null) {
						
						List<String> list = helper.getExistingIDs();
						if (!list.contains(noticeid.get(k).getNoticeDetails())) {
							helper.addnoticeiddetails(noticeid.get(k)
									.getNoticeDetails());
						}
					}
				}
			}
			loading.setVisibility(View.GONE);
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			try {
				noticeBoardUpdates();
				startActivity(new Intent(this, DashboardScreenActivity.class));
				MyNoticeBoardActivity.this.finish();
			} catch (Exception e) {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				MyNoticeBoardActivity.this.finish();
			}
		}
		return super.onKeyDown(keyCode, event);
	}
}
