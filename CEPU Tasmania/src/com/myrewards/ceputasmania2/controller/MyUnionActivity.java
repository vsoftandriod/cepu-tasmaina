package com.myrewards.ceputasmania2.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.ceputasmania2.service.CEPUService;
import com.myrewards.ceputasmania2.service.CEPUServiceListener;
import com.myrewards.ceputasmania2.utils.Utility;

@SuppressLint("SetJavaScriptEnabled")
public class MyUnionActivity extends Activity implements CEPUServiceListener,
		OnClickListener {
	View loading;
	WebView webView;
	Button backButton, scanBarBtn;
	TextView titleTV;;
	String temp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_union);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		
		titleTV=(TextView)findViewById(R.id.titleTVID);
		titleTV.setText(getResources().getString(R.string.my_unions));
		
		scanBarBtn=(Button)findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);	

		webView = (WebView) findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setAllowFileAccess(true);
		webView.getSettings().setLoadsImagesAutomatically(true);

		backButton = (Button) findViewById(R.id.backBtnID);
		backButton.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backButton.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backButton.setOnClickListener(this);

		loading = (View) findViewById(R.id.loading);
		loading.setVisibility(View.GONE);
		if (Utility
				.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			loading.setVisibility(View.VISIBLE);
			CEPUService.getCEPUService().sendTestRequestAbout(this);
		} else {
			
			
			  LayoutInflater inflater = getLayoutInflater();
			   View layout = inflater.inflate(R.layout.toast_no_netowrk,
			   (ViewGroup) findViewById(R.id.custom_toast_layout_id));
			          
			   // The actual toast generated here.
			   Toast toast = new Toast(getApplicationContext());
			   toast.setDuration(Toast.LENGTH_LONG);
			   toast.setView(layout);
			   toast.show();
			
			if (Environment.getExternalStorageState().equals(
					Environment.MEDIA_MOUNTED)) {
				File root = new File(Environment.getExternalStorageDirectory()
						+ "/CEPU");
				File file = new File(root, "myunion.html");
				if (file.exists()) {
					// Do action
					webView.loadUrl("file://"
							+ Environment.getExternalStorageDirectory()
							+ "/CEPU" + "/myunion.html");
				}
			}
		}
	}

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try{
		loading.setVisibility(View.GONE);
		if (response != null) {
			if(eventType!=16)
			{
			String responseSrting = response.toString();
			temp = responseSrting.split("<root>")[0];
			responseSrting = responseSrting.split("<root>")[1];
			responseSrting = responseSrting.replaceFirst("</root>", "</body>");
			String summary = "<html>" + "<body>" + responseSrting + "</html>";
			summary = Html.fromHtml(summary).toString();
			// create text file
			if (!Environment.getExternalStorageState().equals(
					Environment.MEDIA_MOUNTED))
				Log.d("CEPU", "No SDCARD");
			else {
				File direct = new File(
						Environment.getExternalStorageDirectory() + "/CEPU");

				if (!direct.exists()) {
					if (direct.mkdir()) {
						// directory is created;
					}
				}
				try {
					File root = new File(
							Environment.getExternalStorageDirectory() + "/CEPU");
					if (root.canWrite()) {
						File file = new File(root, "myunion.html");
						FileWriter fileWriter = new FileWriter(file);
						BufferedWriter out = new BufferedWriter(fileWriter);
						out.write(summary);
						out.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			if (!Environment.getExternalStorageState().equals(
					Environment.MEDIA_MOUNTED)) {
				Log.d("CEPU", "No SDCARD");
			} else {

				webView.loadUrl("file://"
						+ Environment.getExternalStorageDirectory() + "/CEPU"
						+ "/myunion.html");
			}
		}
		}
		}catch(Exception e){e.printStackTrace();}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.backBtnID) {
			finish();
		}
	}
}
