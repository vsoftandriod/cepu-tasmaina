package com.myrewards.ceputasmania2.controller;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.ceputasmania2.model.Contact;
import com.myrewards.ceputasmania2.utils.DatabaseHelper;
import com.myrewards.ceputasmania2.utils.Utility;

public class MyUnionContactsActivity extends Activity implements
		OnLongClickListener, OnClickListener {
	TextView saTV, emailTV, websiteTV,contactUsTV, phoneOneTV, phoneTwoTV, emailConTV, webConTV;//ntTV
	TextView stv1,stv2,stv3,stv4,stv5,stv6;
	Button addbtn;
	EditText nameET, numberET;
	final int CALL = 1;
	final int REMOVE_CONACT = 2;
	// final int OPEN_WEBSITE = 3;
	ArrayList<Contact> contactsList;
	ListView contactsListView;
	ContactsAdapter mAdapter;
	DatabaseHelper dbHelper;

	Button backBtn, scanBarBtn,okbutton,nobutton;
	TextView titleTV;
	TextView numberTV;
	TextView nameTV;

	public static int position;

	// custom alert boxes for call
	final private static int CALL_CEPU_NUMBER_ONE = 1;
	final private static int OPEN_WEBSITE = 2;
	final private static int ADD_CONTACT = 3;
	final private static int ADD_CONTACT_MAN_FIELD = 4;
	final private static int DELETE_CONTACT = 5;
	final private static int INVALID_NUMBER = 6;
	TextView tv11;
	TextView tv12;
	TextView tv13;
	TextView tv14;
	TextView tv15,addCtTV,myctTV;
	TextView noContactID,alertMsgTV, alertTitleTV;
	String myNumber;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_union_contacts);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		
		titleTV=(TextView)findViewById(R.id.titleTVID);
		titleTV.setText(getResources().getString(R.string.my_union_contacts));
		titleTV.setTypeface(Utility.font_bold);
		
		scanBarBtn=(Button)findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);	

		addCtTV = (TextView) findViewById(R.id.addCtTVID);
		addCtTV.setTypeface(Utility.font_bold);
		
		myctTV = (TextView) findViewById(R.id.myctTVID);
		myctTV.setTypeface(Utility.font_bold);
		
		stv1 = (TextView) findViewById(R.id.phone_noID);
		stv1.setTypeface(Utility.font_bold);
		
		stv2 = (TextView) findViewById(R.id.sa4TVID);
		stv2.setTypeface(Utility.font_bold);
		
		
		 stv3= (TextView) findViewById(R.id.sa2TVID);
		stv3.setTypeface(Utility.font_bold);
		
		
		stv4 = (TextView) findViewById(R.id.contactUSTitleTVID);
		stv4.setTypeface(Utility.font_bold);
		
		stv5= (TextView) findViewById(R.id.sa5TVID);
		stv5.setTypeface(Utility.font_bold);
		
		stv6= (TextView) findViewById(R.id.sa6TVID);
		stv6.setTypeface(Utility.font_bold);
		
		saTV = (TextView) findViewById(R.id.saTVID);
		saTV.setTypeface(Utility.font_bold);
		saTV.setOnClickListener(this);
		//ntTV = (TextView) findViewById(R.id.ntTVID);
		//ntTV.setOnClickListener(this);
		emailTV = (TextView) findViewById(R.id.emailTVID);
		emailTV.setTypeface(Utility.font_bold);
		emailTV.setOnClickListener(this);
		websiteTV = (TextView) findViewById(R.id.websiteTVID);
		websiteTV.setTypeface(Utility.font_bold);
		websiteTV.setOnClickListener(this);
		addbtn = (Button) findViewById(R.id.addbtnID);
		addbtn.getLayoutParams().width = (int) (Utility.screenWidth / 8);
		addbtn.getLayoutParams().height = (int) (Utility.screenHeight / 13.3);
		addbtn.setOnClickListener(this);

		noContactID = (TextView) findViewById(R.id.noContactsAvailableTVID);
		noContactID.setTypeface(Utility.font_bold);
		
		nameET = (EditText) findViewById(R.id.nameETID);
		  nameET.setTypeface(Utility.font_reg);
		  numberET = (EditText) findViewById(R.id.numberETID);
		  numberET.setTypeface(Utility.font_reg);
		nameET.setOnLongClickListener(this);
		numberET.setOnLongClickListener(this);
		dbHelper = new DatabaseHelper(this);
		contactsList = new ArrayList<Contact>();
		loadContacts();
		contactsListView = (ListView) findViewById(R.id.contactsLVID);
		mAdapter = new ContactsAdapter(this);
		contactsListView.setAdapter(mAdapter);
		if (dbHelper.getContactsList().size() == 0) {
			noContactID.setVisibility(View.VISIBLE);
			noContactID.setText("No Contacts Available Here!");
		}

		backBtn = (Button) findViewById(R.id.backBtnID);
		backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backBtn.setOnClickListener(this);
	}

	private void loadContacts() {
		contactsList = dbHelper.getContactsList();
	}

	private void call(String number) {
		// showDialog("CALL",number,CALL,0);

		// custom dialog for Call alerts
		myNumber = number;
		showDialog(CALL_CEPU_NUMBER_ONE);
		// showDialog(CALL_CEPU_NUMBER_TWO);
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.saTVID:
			call(saTV.getText().toString());
			break;
	//	case R.id.ntTVID:
		//	call(ntTV.getText().toString());
	//		break;
		case R.id.websiteTVID:
			// ----------Krishna Alert Dialog------------- //
			// showDialog("OPEN WEBSITE", "Do you want to open CEPU website?",
			// OPEN_WEBSITE, 0);

			// ----------Hari Alert Dialog--------------- //
			showDialog(OPEN_WEBSITE);
			break;
		case R.id.emailTVID:
			String toClientemail = getResources().getString(
					R.string.send_email_hint);
			Intent email = new Intent(Intent.ACTION_SEND);
			email.putExtra(Intent.EXTRA_EMAIL, new String[] { toClientemail });
			email.putExtra(Intent.EXTRA_SUBJECT, "");
			email.putExtra(Intent.EXTRA_TEXT, "");
			// need this to prompts email client only
			email.setType("message/rfc822");
			startActivity(Intent.createChooser(email,
					"Choose an Email client :"));
			break;
		case R.id.addbtnID:
			noContactID.setVisibility(View.GONE);
			addContact();
			break;
		case R.id.backBtnID:
			finish();
			break;
		default:
			break;
		}
	}

	private void addContact() {
		
		String regexStr = "^[0-9]{10}$";
		String CNumber = numberET.getText().toString();
		String CName= nameET.getText().toString();

		// ------------- Hari Alert Dialogs ---------------- //
		if (CName.length() > 1 && CNumber.length() > 1) {
			if (CNumber.matches(regexStr) ) {
				showDialog(ADD_CONTACT);
			} else {
				showDialog(INVALID_NUMBER);
				noContactID.setVisibility(View.VISIBLE);
			}
		} else {
			showDialog(ADD_CONTACT_MAN_FIELD);
			noContactID.setVisibility(View.VISIBLE);			
		}
	}

	public void openWebsite() {
		startActivity(new Intent(MyUnionContactsActivity.this,
				OpenWebSiteActivity.class));
	}

	public void removeContact(int pos) {
		dbHelper.deleteContact(contactsList.get(pos).getNumber());
		contactsList = dbHelper.getContactsList();
		mAdapter.notifyDataSetChanged();
		if (dbHelper.getContactsList().size() == 0) {
			noContactID.setVisibility(View.VISIBLE);
			noContactID.setText("No Contacts Available Here!");
		}
	}

	public class ContactsAdapter extends BaseAdapter {

		public ContactsAdapter(MyUnionContactsActivity resultsListActivity) {

		}

		@Override
		public int getCount() {
			return contactsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(final int pos, View view, ViewGroup arg2) {
			View row = null;
			if (row == null) {
				LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = (View) inflater.inflate(R.layout.contacts_row, null,
						false);
			}
			nameTV = (TextView) row.findViewById(R.id.nameTVID);
			nameTV.setTypeface(Utility.font_bold);
			if (dbHelper.getContactsList().size() == 0) {
				noContactID.setVisibility(View.VISIBLE);
				noContactID.setText("No Contacts Available Here!");
			}
		
			nameTV.setText(contactsList.get(pos).getName());
			numberTV = (TextView) row.findViewById(R.id.numberTVID);
			numberTV.setTypeface(Utility.font_bold);
			numberTV.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(Intent.ACTION_CALL);
					intent.setData(Uri.parse("tel:" + numberTV));
					startActivity(intent);
				}
			});
			nameTV.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(Intent.ACTION_CALL);
					intent.setData(Uri.parse("tel:" + numberTV));
					startActivity(intent);
				}
			});
			numberTV.setText(contactsList.get(pos).getNumber());
			Button deleteBtn = (Button) row.findViewById(R.id.deleteBtnID);
			deleteBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					position = pos;
					showDialog(DELETE_CONTACT);
				}
			});
			return row;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == 1) {
			AlertDialog callOneDialog = null;
			switch (id) {
			case CALL_CEPU_NUMBER_ONE:
				LayoutInflater callInflater1 = LayoutInflater.from(this);
				View callOneView1 = callInflater1.inflate(
						R.layout.dialog_layout_call_one, null);
				AlertDialog.Builder callAlert1 = new AlertDialog.Builder(this);
				callAlert1.setCancelable(false);
				callAlert1.setView(callOneView1);
				callOneDialog = callAlert1.create();
				break;
			}
			return callOneDialog;
		} else if (id == 2) {
			AlertDialog callTwoDialog = null;
			switch (id) {
			case OPEN_WEBSITE:
				LayoutInflater callInflater2 = LayoutInflater.from(this);
				View callOneView2 = callInflater2.inflate(
						R.layout.dialog_layout_open_web_site, null);
				AlertDialog.Builder callAlert2 = new AlertDialog.Builder(this);
				callAlert2.setCancelable(false);
				callAlert2.setView(callOneView2);
				callTwoDialog = callAlert2.create();
				break;
			}
			return callTwoDialog;
		} else if (id == 3) {
			AlertDialog contactAddDialog = null;
			switch (id) {
			case ADD_CONTACT:
				LayoutInflater callInflater2 = LayoutInflater.from(this);
				View callOneView2 = callInflater2.inflate(
						R.layout.dialog_layout_call_add_contact, null);
				AlertDialog.Builder callAlert2 = new AlertDialog.Builder(this);
				callAlert2.setCancelable(false);
				callAlert2.setView(callOneView2);
				contactAddDialog = callAlert2.create();
				break;
			}
			return contactAddDialog;
		} else if (id == 4) {
			AlertDialog mandatoryFieldsDialog = null;
			switch (id) {
			case ADD_CONTACT_MAN_FIELD:
				LayoutInflater callInflater2 = LayoutInflater.from(this);
				View callOneView2 = callInflater2.inflate(
						R.layout.dialog_layout_call_man_fileds, null);
				AlertDialog.Builder callAlert2 = new AlertDialog.Builder(this);
				callAlert2.setCancelable(false);
				callAlert2.setView(callOneView2);
				mandatoryFieldsDialog = callAlert2.create();
				break;
			}
			return mandatoryFieldsDialog;
		} else if (id == 5) {
			AlertDialog mandatoryFieldsDialog = null;
			switch (id) {
			case DELETE_CONTACT:
				LayoutInflater callInflater2 = LayoutInflater.from(this);
				View callOneView2 = callInflater2.inflate(
						R.layout.dialog_layout_delete_contact_call, null);
				AlertDialog.Builder callAlert2 = new AlertDialog.Builder(this);
				callAlert2.setCancelable(false);
				callAlert2.setView(callOneView2);
				mandatoryFieldsDialog = callAlert2.create();
				break;
			}
			return mandatoryFieldsDialog;
		}else if (id == 6) {
			AlertDialog mandatoryFieldsDialog = null;
			switch (id) {
			case INVALID_NUMBER:
				LayoutInflater callInflater2 = LayoutInflater.from(this);
				View callOneView2 = callInflater2.inflate(
						R.layout.dialog_layout_call_man_fileds, null);
				AlertDialog.Builder callAlert2 = new AlertDialog.Builder(this);
				callAlert2.setCancelable(false);
				callAlert2.setView(callOneView2);
				mandatoryFieldsDialog = callAlert2.create();
				break;
			}
			return mandatoryFieldsDialog;
		}
		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		if (id == 1) {
			switch (id) {
			case CALL_CEPU_NUMBER_ONE:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				alertTitleTV = (TextView)alertDialog1.findViewById(R.id.firstLoginCEPUTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				alertMsgTV = (TextView) alertDialog1.findViewById(R.id.callNumberOneTVID);
				alertMsgTV.setTypeface(Utility.font_reg);
				alertMsgTV.setText(myNumber);
				okbutton = (Button) alertDialog1.findViewById(R.id.call_one_yesBtnID);
				okbutton.setTypeface(Utility.font_bold);
				nobutton = (Button) alertDialog1.findViewById(R.id.call_one_noBtnID);
				nobutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(Intent.ACTION_CALL);
						intent.setData(Uri.parse("tel: "+myNumber));
						startActivity(intent);
						alertDialog1.dismiss();
					}
				});
				nobutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
				break;
			}
		} else if (id == 2) {
			switch (id) {
			case OPEN_WEBSITE:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				alertTitleTV = (TextView)alertDialog1.findViewById(R.id.firstLoginCEPUTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				alertMsgTV = (TextView) alertDialog1.findViewById(R.id.openWebSiteTVID);
				alertMsgTV.setTypeface(Utility.font_reg);
				ImageView webIconIV = (ImageView) alertDialog1.findViewById(R.id.webImgIVID);
				webIconIV.getLayoutParams().width = (int) (Utility.screenWidth / 12.0);
				webIconIV.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
				okbutton = (Button) alertDialog1.findViewById(R.id.open_web_yesBtnID);
				okbutton.setTypeface(Utility.font_bold);
				nobutton = (Button) alertDialog1.findViewById(R.id.open_web_noBtnID);
				nobutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
						openWebsite();
						}
						else
						{
							   LayoutInflater inflater = getLayoutInflater();
							   View layout = inflater.inflate(R.layout.toast_no_netowrk,
							   (ViewGroup) findViewById(R.id.custom_toast_layout_id));
							          
							   // The actual toast generated here.
							   Toast toast = new Toast(getApplicationContext());
							   toast.setDuration(Toast.LENGTH_LONG);
							   toast.setView(layout);
							   toast.show();
						}
						alertDialog1.dismiss();
					}
				});
				nobutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
				break;
			}
		} else if (id == 3) {
			switch (id) {
			case ADD_CONTACT:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				alertTitleTV = (TextView)alertDialog1.findViewById(R.id.firstLoginCEPUTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				alertMsgTV = (TextView) alertDialog1.findViewById(R.id.addContactTVID);
				alertMsgTV.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog1.findViewById(R.id.addContactOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dbHelper.addContact(new Contact(nameET.getText()
								.toString(), numberET.getText().toString()));
						contactsList = dbHelper.getContactsList();
						mAdapter.notifyDataSetChanged();
						nameET.setText("");
						numberET.setText("");
						alertDialog1.dismiss();
					}
				});
				break;
			}
		} else if (id == 4) {
			switch (id) {
			case ADD_CONTACT_MAN_FIELD:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				alertTitleTV = (TextView)alertDialog1.findViewById(R.id.alertsTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				// set the dialog message
				alertMsgTV = (TextView) alertDialog1.findViewById(R.id.manFieldTVID);
				alertMsgTV.setTypeface(Utility.font_reg);
				// set the ok button clicking action
				okbutton = (Button) alertDialog1.findViewById(R.id.manFieldOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (!(contactsList.size() == 0)) {
							noContactID.setVisibility(View.GONE);
						}
						alertDialog1.dismiss();
					}
				});
				break;
			}
		} else if (id == 5) {
			switch (id) {
			case DELETE_CONTACT:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				alertTitleTV = (TextView)alertDialog1.findViewById(R.id.firstLoginCEPUTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				alertMsgTV = (TextView) alertDialog1.findViewById(R.id.deleteContactTVID);
				alertMsgTV.setTypeface(Utility.font_reg);
				alertMsgTV.setText("Do you want to remove "
						+ contactsList.get(position).getName()
						+ " from contacts?");
				okbutton = (Button) alertDialog1.findViewById(R.id.delete_one_yesBtnID);
				okbutton.setTypeface(Utility.font_bold);
				nobutton = (Button) alertDialog1.findViewById(R.id.delete_one_noBtnID);
				nobutton.setTypeface(Utility.font_bold);
				
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						removeContact(position);
						alertDialog1.dismiss();
					}
				});
				nobutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
				break;
			}
		} else if (id == 6) {
			switch (id) {
			case INVALID_NUMBER:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				alertTitleTV = (TextView) alertDialog1.findViewById(R.id.alertsTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				alertTitleTV.setText("Invalid Number !");
				TextView alertMsg2TV = (TextView) alertDialog1.findViewById(R.id.manFieldTVID);
				alertMsg2TV.setTypeface(Utility.font_reg);
				alertMsg2TV.setText("Please enter 10 digit mobile number.");
				// set the ok button clicking action
				okbutton = (Button) alertDialog1.findViewById(R.id.manFieldOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (!(contactsList.size() == 0)) {
							noContactID.setVisibility(View.GONE);
						}
						alertDialog1.dismiss();
					}
				});
				break;
			}
		}
	}

	@Override
	public boolean onLongClick(View v) {
		boolean returnValue = true;
		if (v.getId() == R.id.nameETID) {
			if (nameET.getText().length() > 25) {
				returnValue = true;

			} else {
				returnValue = false;
			}
		} else if (v.getId() == R.id.numberETID) {
			if (numberET.getText().length() > 25) {
				returnValue = true;

			} else {
				returnValue = false;
			}
		}
		return returnValue;
	}
}
