package com.myrewards.ceputasmania2.controller;

import java.util.ArrayList;
import java.util.List;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.util.Log;

import com.myrewards.ceputasmania2.model.NoticeId;
import com.myrewards.ceputasmania2.service.CEPUServiceListener;
import com.myrewards.ceputasmania2.service.GrabItNowService;
import com.myrewards.ceputasmania2.utils.DatabaseHelper;
import com.myrewards.ceputasmania2.utils.Utility;

public class NoticeBoardReceiver extends BroadcastReceiver implements
		CEPUServiceListener {
	ArrayList<NoticeId> noticeid;
	DatabaseHelper dbHelper;
	Context context;
	String noticeMessages = "";

	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			this.context = context;
			dbHelper = new DatabaseHelper(context);
			if (Utility.isOnline((ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE)))
				GrabItNowService.getGrabItNowService()
						.sendNoticeBoardCountIdRequset(this);		
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
	}

	private void sendNotification() {
		try {
			final NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			final Notification msg = new Notification(R.drawable.my_notice,	"Noticeboard Updates", System.currentTimeMillis());
			msg.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			Intent in = new Intent(context, DashboardScreenActivity.class);

			PendingIntent pin = PendingIntent.getActivity(context, 0, in, 0);
			msg.setLatestEventInfo(context, "Noticeboard Updates",
					"New Message From Noticeboard !", pin);
			msg.flags = Notification.FLAG_AUTO_CANCEL;
			manager.notify(1, msg);
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		if (eventType == 16) {
			if (response != null) {
				try {
					noticeid = (ArrayList<NoticeId>) response;
					int i = -1;
					if (noticeid != null)
					{
						List<String> noticesList = new ArrayList<String>();
						for (int n = 0; n < noticeid.size(); n++) {
							if (!(noticeid.get(n).getNoticeDetails() == null))
								noticesList.add(noticeid.get(n)
										.getNoticeDetails());
						}
						if (noticesList.size() != dbHelper.getExistingIDs()
								.size()) {
							for (int m = 0; m < dbHelper.getExistingIDs()
									.size(); m++) {
								if (noticesList.contains(dbHelper
										.getExistingNoticeRealIDs().get(m))) {

								} else {
									dbHelper.deleteNoticeIdDetails(dbHelper
											.getExistingNoticeRealIDs().get(m));

								}
							}
							if (noticesList.size() > dbHelper.getExistingIDs().size()) {
								sendNotification();
							}
						}

						for (int k = 0; k < noticeid.size(); k++) {
							if (noticeid.get(k).getNoticeDetails() != null) {
								i++;
								List<String> list = dbHelper.getExistingIDs();
								if (!list.contains(noticeid.get(k)
										.getNoticeDetails())) {
									dbHelper.addnoticeiddetails(noticeid.get(k)
											.getNoticeDetails());
								}
							}
						}
						Log.v("Hari....>", "Count = " + i);

					}
				} catch (Exception e) {
					if (e != null) {
						Log.w("HARI-->Debug", e);
						e.printStackTrace();
					}
				}
			}

		}

	}

}
