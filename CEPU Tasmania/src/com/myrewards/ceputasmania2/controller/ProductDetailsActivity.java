package com.myrewards.ceputasmania2.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager.LayoutParams;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.ceputasmania2.cache.SmartImageView;
import com.myrewards.ceputasmania2.model.Product;
import com.myrewards.ceputasmania2.model.ProductAddress;
import com.myrewards.ceputasmania2.model.ProductDetails;
import com.myrewards.ceputasmania2.service.CEPUServiceListener;
import com.myrewards.ceputasmania2.service.GrabItNowService;
import com.myrewards.ceputasmania2.utils.ApplicationConstants;
import com.myrewards.ceputasmania2.utils.DatabaseHelper;
import com.myrewards.ceputasmania2.utils.Utility;

@SuppressLint({ "CutPasteId", "SetJavaScriptEnabled" })
public class ProductDetailsActivity extends BaseActivity implements
		CEPUServiceListener, OnClickListener {
	ProductDetails product;
	RelativeLayout offerDetailsRL;
	RelativeLayout tcDetailsRL;
	public static int productId = -1;
	String productName = null;
	String productHighlight = null;
	String dailyDealsImage = null;
	DatabaseHelper dbHelper;
	public static boolean isProductStored = false;
	public static RelativeLayout favoritesRL;
	View loading;
	ImageView productLogoIV;
	Drawable productLogoDrawable;
	boolean isClientbanner = true;
	Button t_cBtn;
	public static boolean contactboolean = false;
	ListView addressContactsListView, addressContactsListViewTWO;
	MyAddressAdapter mAdapter, mAdapter2;
	List<String> contactsList;
	Context context;
	List<ProductAddress> productAddressList;
	List<ProductAddress> productAddressTempList;
	// this is for daily deals product
	List<Product> productfav;
	boolean isAddressList = false;
	String productImageURL;
	Bitmap bitmap9;
	final private static int REDEEM_BUTTON = 1;
	final private static int REDEEM_YES_BUTTON = 2;
	final private static int FAVOURITE_ADD_BUTTON = 3;
	final private static int FAVOURITE_REMOVE_BUTTON = 4;
	final private static int NO_ADDRESS = 5;
	final private static int CALL_NUMBER = 6;
	private static int position = 0;
	public TextView tv11;
	public Button rdmYes;

	Button moneyDetailsBtn;
	Button mapDetailsDtn;
	Button contactDetailsBtn;
	Button saleDetailsBtn;
	Button offerBackBtn;
	Button redeemBtn, redeemBtn2;
	Button backBtn;// back to t & c view
	ScrollView TandCSV;

	public static TextView addressTV;
	public static ImageView mapViewIV;
	// this textview is for starting heading in the list text
	TextView detailsTV2, detailsTV, mapTextNameTV;

	RelativeLayout coupanRelativeLayout;

	// add favorite custom dialog
	public Button addFavYesBtn, addNoFavBtn;
	public TextView tv22;

	// Call custom dialog....
	public Button callYesBtn, callNoBtn;
	public TextView callNumber;
	TextView tcDetailsTV, tandCTitleTV;

	// redeem custom dialog......
	public static TextView text1, text2, text3, alertTitleTV;

	public static int dailyDealsID;
	ImageView logoIV;
	public static String dailydealsPname, dailydealsPnametwo, dealsImage;
	
	//imageLoading
	String productLogoRCouponLogo = null;

	@SuppressWarnings("static-access")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.result_details);
		RelativeLayout headerRL = (RelativeLayout) findViewById(R.id.headerRLID);
		headerRL.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		headerRL.setVisibility(View.VISIBLE);
		View header1 = (View) findViewById(R.id.header);
		header1.setVisibility(View.VISIBLE);

		logoIV = (ImageView) findViewById(R.id.productLogoIVID);
		logoIV.getLayoutParams().height = (int) (Utility.screenHeight / 4.8f);
		logoIV.getLayoutParams().width = (int) (Utility.screenWidth / 2.88f);

		// this is coupon's listview text heading textview
		detailsTV2 = (TextView) findViewById(R.id.productDetailsTabTVID2);
		detailsTV2.setTypeface(Utility.font_bold);

		detailsTV = (TextView) findViewById(R.id.productDetailsTabTVID);
		detailsTV.setTypeface(Utility.font_bold);

		coupanRelativeLayout = (RelativeLayout) findViewById(R.id.offerDetailsRLID);
		coupanRelativeLayout.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				offerDetailsRL.setVisibility(RelativeLayout.GONE);
				enableDisableButtons("enable");
				return false;
			}
		});

		setHeaderTitle(getResources().getString(R.string.search_products_text));
		showBackButton();
		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		loading = (View) findViewById(R.id.loading);
		addressContactsListView = (ListView) findViewById(R.id.addressesLVID);
		addressContactsListViewTWO = (ListView) findViewById(R.id.addressesLVIDTWO);
		context = this;
		dbHelper = new DatabaseHelper(this);
		// initialiseViews();

		/*moneyDetailsBtn = (Button) findViewById(R.id.moneyDetailsBtnID);
		mapDetailsDtn = (Button) findViewById(R.id.mapDetailsBtnID);
		contactDetailsBtn = (Button) findViewById(R.id.contactDetailsBtnID);
		saleDetailsBtn = (Button) findViewById(R.id.saleDetailsBtnID);*/

		RelativeLayout productLogoOneRL = (RelativeLayout) findViewById(R.id.productLogoOneRLID);
		int widths = (int) (Utility.screenWidth / 2);
		int heights = (int) (Utility.screenHeight / 5);
		productLogoOneRL.getLayoutParams().width = widths;
		productLogoOneRL.getLayoutParams().height = heights;
		
		RelativeLayout iconsRL = (RelativeLayout) findViewById(R.id.iconsRLID);
		iconsRL.getLayoutParams().width = widths;
		iconsRL.getLayoutParams().height = heights;

		moneyDetailsBtn = (Button) findViewById(R.id.moneyDetailsBtnID);
		moneyDetailsBtn.getLayoutParams().width = (int) ((widths / 2) - 11);
		moneyDetailsBtn.getLayoutParams().height = (int) ((heights / 2) - 11);

		mapDetailsDtn = (Button) findViewById(R.id.mapDetailsBtnID);
		mapDetailsDtn.getLayoutParams().width = (int) ((widths / 2) - 11);
		mapDetailsDtn.getLayoutParams().height = (int) ((heights / 2) - 11);

		contactDetailsBtn = (Button) findViewById(R.id.contactDetailsBtnID);
		contactDetailsBtn.getLayoutParams().width = (int) ((widths / 2) - 11);
		contactDetailsBtn.getLayoutParams().height = (int) ((heights / 2) - 11);

		saleDetailsBtn = (Button) findViewById(R.id.saleDetailsBtnID);
		saleDetailsBtn.getLayoutParams().width = (int) ((widths / 2) - 11);
		saleDetailsBtn.getLayoutParams().height = (int) ((heights / 2) - 11);

		// Relative
		RelativeLayout moneyDetailsRL = (RelativeLayout) findViewById(R.id.moneyDetailsRLID);
		moneyDetailsRL.getLayoutParams().width = (int) ((widths / 2) - 1.14);
		moneyDetailsRL.getLayoutParams().height = (int) (heights / 2);

		RelativeLayout mapDetailsRL = (RelativeLayout) findViewById(R.id.mapDetailsRLID);
		mapDetailsRL.getLayoutParams().width = (int) ((widths / 2) + 1.14);
		mapDetailsRL.getLayoutParams().height = (int) (heights / 2);

		RelativeLayout contactDetailsRL = (RelativeLayout) findViewById(R.id.contactDetailsRLID);
		contactDetailsRL.getLayoutParams().width = (int) ((widths / 2) - 1.14);
		contactDetailsRL.getLayoutParams().height = (int) (heights / 2);

		RelativeLayout saleDetailsRL = (RelativeLayout) findViewById(R.id.saleDetailsRLID);
		saleDetailsRL.getLayoutParams().width = (int) ((widths / 2) + 1.14);
		saleDetailsRL.getLayoutParams().height = (int) (heights / 2);

		favoritesRL = (RelativeLayout) findViewById(R.id.markFavouriteRLID);
		favoritesRL.getLayoutParams().width = (int) (Utility.screenWidth / 11);
		favoritesRL.getLayoutParams().height = (int) (Utility.screenHeight / 17.6);

		t_cBtn = (Button) findViewById(R.id.tcBtnID);

		RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params1.setMargins(0, 0, (int) (Utility.screenWidth / 8),
				(int) (Utility.screenWidth / 12));
		params1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		params1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params1.addRule(Gravity.RIGHT);
		params1.width = (int) (Utility.screenWidth / 5.6);
		params1.height = (int) (Utility.screenHeight / 18.7);
		t_cBtn.setLayoutParams(params1);

		redeemBtn = (Button) findViewById(R.id.redeemBtnID);

		RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params2.setMargins(0, 0, (int) (Utility.screenWidth / 8),
				(int) (Utility.screenWidth / 12));
		params2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		params2.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params2.addRule(Gravity.RIGHT);
		params2.width = (int) (Utility.screenWidth / 5.6);
		params2.height = (int) (Utility.screenHeight / 18.7);
		redeemBtn.setLayoutParams(params2);

		redeemBtn2 = (Button) findViewById(R.id.redeemBtnID2);

		RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params3.setMargins((int) (Utility.screenWidth / 8), 0, 0,
				(int) (Utility.screenWidth / 12));
		params3.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		params3.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		params3.addRule(Gravity.LEFT);
		params3.width = (int) (Utility.screenWidth / 5.6);
		params3.height = (int) (Utility.screenHeight / 18.7);
		redeemBtn2.setLayoutParams(params3);

		backBtn = (Button) findViewById(R.id.tc_backBtnID);

		RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params4.setMargins((int) (Utility.screenWidth / 8), 0, 0,
				(int) (Utility.screenWidth / 12));
		params4.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		params4.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		params4.addRule(Gravity.LEFT);
		params4.width = (int) (Utility.screenWidth / 5.6);
		params4.height = (int) (Utility.screenHeight / 18.7);
		backBtn.setLayoutParams(params4);

		TandCSV = (ScrollView) findViewById(R.id.ptextSVID);
		RelativeLayout.LayoutParams params5 = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params5.setMargins((int) (Utility.screenWidth / 8),
				(int) (Utility.screenWidth / 5.6),
				(int) (Utility.screenWidth / 8),
				(int) (Utility.screenWidth / 5.6));
		// params5.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		params5.width = params5.WRAP_CONTENT;
		params5.height = (int) (Utility.screenHeight / 2.0);
		TandCSV.setLayoutParams(params5);

		tandCTitleTV = (TextView) findViewById(R.id.tcApplay);
		tandCTitleTV.setTypeface(Utility.font_bold);

		RelativeLayout.LayoutParams paramsTC = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		paramsTC.setMargins(0, Utility.screenWidth / 12, 0, 0);
		paramsTC.addRule(RelativeLayout.CENTER_HORIZONTAL);
		tandCTitleTV.setLayoutParams(paramsTC);

		tcDetailsTV = (TextView) findViewById(R.id.tcDetailsTVID);
		tcDetailsTV.setTypeface(Utility.font_reg);

		moneyDetailsBtn.setOnClickListener(this);
		mapDetailsDtn.setOnClickListener(this);
		contactDetailsBtn.setOnClickListener(this);
		saleDetailsBtn.setOnClickListener(this);
		
		saleDetailsBtn.setEnabled(false);
		mapDetailsDtn.setEnabled(false);
		moneyDetailsBtn.setEnabled(false);
		contactDetailsBtn.setEnabled(false);
		
		redeemBtn.setEnabled(false);
		redeemBtn2.setEnabled(false);
		backBtn.setEnabled(false);
		
		t_cBtn.setOnClickListener(this);
		// offerBackBtn.setOnClickListener(this);
		backBtn.setOnClickListener(this);
		redeemBtn.setOnClickListener(this);
		redeemBtn2.setOnClickListener(this);
	
		favoritesRL.setEnabled(false);
		favoritesRL.setOnClickListener(this);
		offerDetailsRL = (RelativeLayout) findViewById(R.id.offerDetailsRLID);
		tcDetailsRL = (RelativeLayout) findViewById(R.id.tc_detailsRLID);
		offerDetailsRL.setVisibility(RelativeLayout.GONE);
		tcDetailsRL.setVisibility(RelativeLayout.GONE);
		// productLogoIV = (ImageView)findViewById(R.id.productLogoIVID);
		if (getIntent() != null) {
			try {
				int colorResource = 0;
				Bundle bundle = getIntent().getExtras();
				dailyDealsImage = bundle
						.getString(ApplicationConstants.DAILYDEALSIMAGE);
				if (bundle != null) {
					if (bundle.getString(ApplicationConstants.PRODUCT_NAME_KEY) != null
							&& bundle
									.getString(ApplicationConstants.PRODUCT_HIGHLIGHT_KEY) != null) {
						productId = bundle
								.getInt(ApplicationConstants.PRODUCT_ID_KEY);

						if (Utility
								.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							GrabItNowService.getGrabItNowService()
									.sendProductDetailsRequest(this, productId);
						} else {
							loading.setVisibility(View.GONE);
							// The Custom Toast Layout Imported here
							LayoutInflater inflater = getLayoutInflater();
							View layout = inflater
									.inflate(
											R.layout.toast_no_netowrk,
											(ViewGroup) findViewById(R.id.custom_toast_layout_id));

							// The actual toast generated here.
							Toast toast = new Toast(getApplicationContext());
							toast.setDuration(Toast.LENGTH_LONG);
							toast.setView(layout);
							toast.show();
							ProductDetailsActivity.this.finish();
						}

						productName = bundle
								.getString(ApplicationConstants.PRODUCT_NAME_KEY);
						productHighlight = bundle
								.getString(ApplicationConstants.PRODUCT_HIGHLIGHT_KEY);
						colorResource = bundle
								.getInt(ApplicationConstants.COLOR_CODE_KEY);
						productImageURL = bundle
								.getString(ApplicationConstants.IMAGE_URL_KEY);
					} else {
						productId = bundle
								.getInt(ApplicationConstants.PRODUCT_ID_KEY);

						if (Utility
								.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							GrabItNowService.getGrabItNowService()
									.sendProductDetailsRequest(this, productId);
						} else {
							loading.setVisibility(View.GONE);
							// The Custom Toast Layout Imported here
							LayoutInflater inflater = getLayoutInflater();
							View layout = inflater
									.inflate(
											R.layout.toast_no_netowrk,
											(ViewGroup) findViewById(R.id.custom_toast_layout_id));

							// The actual toast generated here.
							Toast toast = new Toast(getApplicationContext());
							toast.setDuration(Toast.LENGTH_LONG);
							toast.setView(layout);
							toast.show();
							ProductDetailsActivity.this.finish();
						}

						if (dailydealsPname != null && dailydealsPnametwo != null) {
							productName = dailydealsPname;
							productHighlight = dailydealsPnametwo;
						}
						colorResource = bundle
								.getInt(ApplicationConstants.COLOR_CODE_KEY);
						productImageURL = bundle
								.getString(ApplicationConstants.IMAGE_URL_KEY);
						dailyDealsID = productId;
					}

				}
				View resultListItem = (View) findViewById(R.id.resultListItemID);
				resultListItem.getLayoutParams().height = (int) (Utility.screenHeight / 8.5);
				isProductStored = dbHelper.isProductExist(Integer
						.toString(productId));
				if (isProductStored) {
					favoritesRL.setBackgroundResource(R.drawable.heart_full);
				} else {
					favoritesRL.setBackgroundResource(R.drawable.heart_empty);
				}
				TextView productNameTV = (TextView) findViewById(R.id.productTVID);
				productNameTV.setTypeface(Utility.font_bold);
				TextView highlightTV = (TextView) findViewById(R.id.offerTVID);
				highlightTV.setTypeface(Utility.font_reg);
				if(productName!=null)
				{
				productNameTV.setText(productName);
				}
				if(productHighlight!=null)
				{
				highlightTV.setText(productHighlight);
				}
				switch (colorResource) {
				case 0:
					resultListItem.setBackgroundResource(R.color.result_color_one);
					productNameTV.setTextColor(Color.parseColor("#2E52AC"));

					break;
				case 1:
					resultListItem.setBackgroundResource(R.color.result_color_two);
					productNameTV.setTextColor(Color.parseColor("#60B3FF"));
					break;
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("DEBUG-->", e);
					e.printStackTrace();
				}
			}
		}
	}
	private void enableDisableButtons(String mode)
	{
		if(mode.equals("enable"))
		{
			saleDetailsBtn.setEnabled(true);
			mapDetailsDtn.setEnabled(true);
			moneyDetailsBtn.setEnabled(true);
			contactDetailsBtn.setEnabled(true);
			favoritesRL.setEnabled(true);
		}
		else
		{
			saleDetailsBtn.setEnabled(false);
			mapDetailsDtn.setEnabled(false);
			moneyDetailsBtn.setEnabled(false);
			contactDetailsBtn.setEnabled(false);
			favoritesRL.setEnabled(false);
		}
	}
	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (eventType != 16) {
				if (response != null) {
					if (response instanceof String) {
						if (response.toString().equalsIgnoreCase("success\n")) {
							showRedeemThanxDialog();
						} else {
							Utility.showMessage(this, response.toString());
						}
					} else if (response instanceof ProductDetails) {
						product = (ProductDetails) response;
						
						if (product != null) {
							saleDetailsBtn.setEnabled(true);
							mapDetailsDtn.setEnabled(true);
							moneyDetailsBtn.setEnabled(true);
							contactDetailsBtn.setEnabled(true);
							favoritesRL.setEnabled(true);
							redeemBtn.setEnabled(true);
							redeemBtn2.setEnabled(true);
							backBtn.setEnabled(true);
						}
						if (product.getId() == dailyDealsID) {
							productName = product.getName();
							productHighlight = product.getHighlight();
						}
						
						productLogoRCouponLogo = "productLogo";
						productLogosImagesLoading(productLogoRCouponLogo);
						
						TextView productNameTV = (TextView) findViewById(R.id.productTVID);
						productNameTV.setTypeface(Utility.font_bold);
						TextView highlightTV = (TextView) findViewById(R.id.offerTVID);
						highlightTV.setTypeface(Utility.font_bold);
						if(product.getName()!=null)
						{
						productNameTV.setText(product.getName());
						}
						if(product.getHighlight()!=null)
						{
						highlightTV.setText(product.getHighlight());
						}
						if (product.getText() != null) {
							String temp = product.getDetails()
									+ product.getText();
							if(product.getHighlight()!=null)
							{
							detailsTV2.setText(product.getHighlight() + ":");
							}

							if (product.getQuantity() == 1) {
								detailsTV.setText("Shop Now");
								detailsTV.setPaintFlags(detailsTV
										.getPaintFlags()
										| Paint.UNDERLINE_TEXT_FLAG);

								detailsTV.setVisibility(View.VISIBLE);
								detailsTV.setOnClickListener(new OnClickListener() {

											@Override
											public void onClick(View v) {
												// String url =
												// "http://www.perksatwork.com.au/?user="+Utility.user.getUsername()+"&pageid="+product.getQuantity();
												String url = "http://www.perksatwork.com.au/index.html?user="
														+ Utility.user
																.getUsername();
												Intent i = new Intent(
														Intent.ACTION_VIEW);
												i.setData(Uri.parse(url));
												startActivity(i);

											}
										});
							}

							WebView webView = (WebView) findViewById(R.id.webview);
							webView.getSettings().setJavaScriptEnabled(true);
							webView.getSettings().setAllowFileAccess(true);
							webView.getSettings().setLoadsImagesAutomatically(
									true);
							WebSettings settings = webView.getSettings();
							settings.setDefaultTextEncodingName("utf-8");
							webView.setWebViewClient(new WebViewClient() {

								@Override
								public boolean shouldOverrideUrlLoading(
										WebView view, String url) {
									if (url.startsWith("tel:")) {
										Intent intent = new Intent(
												Intent.ACTION_DIAL, Uri
														.parse(url));
										startActivity(intent);
									} else if (url.startsWith("http:")
											|| url.startsWith("https:")) {
										Intent intent = new Intent(
												Intent.ACTION_VIEW, Uri
														.parse(url));
										startActivity(intent);
									} else if (url.startsWith("mailto:")) {
										MailTo mt = MailTo.parse(url);
										Intent i = hariEmailIntent(
												ProductDetailsActivity.this,
												mt.getTo(), mt.getSubject(),
												mt.getBody(), mt.getCc());
										startActivity(i);
										view.reload();
										return true;
									} else {
										view.loadUrl(url);
									}
									return true;
								}
							});
							String summary = "<html><body style=\"font-family:Helvetica;line-height:20px\">"
									+ product.getDetails()
									+ "</br>"
									+ product.getText() + "</body></html>";

							summary = summary.replaceAll("//", "");
							// create text file
							if (!Environment.getExternalStorageState().equals(
									Environment.MEDIA_MOUNTED))
								Log.d("CEPU", "No SDCARD");
							else {
								File direct = new File(
										Environment
												.getExternalStorageDirectory()
												+ "/CEPU");

								if (!direct.exists()) {
									if (direct.mkdir()) {
										// directory is created;
									}
								}

								try {
									File root = new File(
											Environment
													.getExternalStorageDirectory()
													+ "/CEPU");
									if (root.canWrite()) {
										File file = new File(root,
												"CEPUproductdetails.html");
										FileWriter fileWriter = new FileWriter(
												file);
										BufferedWriter out = new BufferedWriter(
												fileWriter);

										if (summary.contains("<iframe")) {
											try {
												int a = summary
														.indexOf("<iframe");
												int b = summary
														.indexOf("</iframe>");
												summary = summary.replace(
														summary.subSequence(a,
																b), "");
											} catch (Exception e) {
												e.printStackTrace();
											}
										}

										out.write(summary);
										out.close();
									}
								} catch (IOException e) {
									e.printStackTrace();
								}
							}

							if (!Environment.getExternalStorageState().equals(
									Environment.MEDIA_MOUNTED)) {
								Log.d("CEPU", "No SDCARD");
							} else {

								webView.loadUrl("file://"
										+ Environment
												.getExternalStorageDirectory()
										+ "/CEPU" + "/CEPUproductdetails.html");
								// }

								webView.setVisibility(View.VISIBLE);
							}
							detailsTV2.setVisibility(View.VISIBLE);
							loading.setVisibility(View.GONE);
							applyMenuListSlideAnimation();
						} else {
							if (product.getDetails() != null) {
								if(product.getHighlight()!=null)
								{
								detailsTV2
										.setText(product.getHighlight() + ":");
								}
								WebView webView = (WebView) findViewById(R.id.webview);
								webView.getSettings()
										.setJavaScriptEnabled(true);
								webView.getSettings().setAllowFileAccess(true);
								webView.getSettings()
										.setLoadsImagesAutomatically(true);
								WebSettings settings = webView.getSettings();
								settings.setDefaultTextEncodingName("utf-8");
								webView.setWebViewClient(new WebViewClient() {

									@Override
									public boolean shouldOverrideUrlLoading(
											WebView view, String url) {
										if (url.startsWith("tel:")) {
											Intent intent = new Intent(
													Intent.ACTION_DIAL, Uri
															.parse(url));
											startActivity(intent);
										} else if (url.startsWith("http:")
												|| url.startsWith("https:")) {
											Intent intent = new Intent(
													Intent.ACTION_VIEW, Uri
															.parse(url));
											startActivity(intent);
										} else if (url.startsWith("mailto:")) {
											MailTo mt = MailTo.parse(url);
											Intent i = hariEmailIntent(
													ProductDetailsActivity.this,
													mt.getTo(), mt.getSubject(),
													mt.getBody(), mt.getCc());
											startActivity(i);
											view.reload();
											return true;
										} else {
											view.loadUrl(url);
										}
										return true;
									}
								});
								String summary = "<html><body style=\"font-family:Helvetica;line-height:20px\">"
										+ product.getDetails()
										+ "</body></html>";

								summary = summary.replaceAll("//", "");
								// create text file
								if (!Environment.getExternalStorageState()
										.equals(Environment.MEDIA_MOUNTED))
									Log.d("CEPU", "No SDCARD");
								else {
									File direct = new File(
											Environment
													.getExternalStorageDirectory()
													+ "/CEPU");

									if (!direct.exists()) {
										if (direct.mkdir()) {
											// directory is created;
										}

									}

									try {
										File root = new File(
												Environment
														.getExternalStorageDirectory()
														+ "/CEPU");
										if (root.canWrite()) {
											File file = new File(root,
													"CEPUproductdetails.html");
											FileWriter fileWriter = new FileWriter(
													file);
											BufferedWriter out = new BufferedWriter(
													fileWriter);

											if (summary.contains("<iframe")) {
												try {
													int a = summary
															.indexOf("<iframe");
													int b = summary
															.indexOf("</iframe>");
													summary = summary
															.replace(
																	summary.subSequence(
																			a,
																			b),
																	"");
												} catch (Exception e) {
													e.printStackTrace();
												}
											}

											out.write(summary);
											out.close();
										}
									} catch (IOException e) {
										e.printStackTrace();
									}
								}

								if (!Environment.getExternalStorageState()
										.equals(Environment.MEDIA_MOUNTED)) {
									Log.d("CEPU", "No SDCARD");
								} else {

									webView.loadUrl("file://"
											+ Environment
													.getExternalStorageDirectory()
											+ "/CEPU"
											+ "/CEPUproductdetails.html");
									// }

									webView.setVisibility(View.VISIBLE);
								}
								detailsTV2.setVisibility(View.VISIBLE);
								loading.setVisibility(View.GONE);
								applyMenuListSlideAnimation();
							}
						}
					} else {
						productAddressTempList = (List<ProductAddress>) response;
						productAddressList=new ArrayList<ProductAddress>();
						mAdapter = new MyAddressAdapter(this, R.layout.addresses_list_item);
						for(int i=0;i<productAddressTempList.size();i++)
						{
							if(productAddressTempList.get(i).getAddress1()!=null)
							{
								productAddressList.add(productAddressTempList.get(i));
							}
						}
						if(productAddressList.size()==0)
						{
							productAddressList.add(new ProductAddress());
						}
						addressContactsListView.setAdapter(mAdapter);
						addressContactsListView
								.setOnItemClickListener(new OnItemClickListener() {
									@Override
									public void onItemClick(
											AdapterView<?> arg0, View rowView,
											int pos, long arg3) {

										try {

											if (productAddressList.get(pos).getLatitude() != null && !addressTV.getText().equals("No Address Available Here:")) {
												double lat = Double.parseDouble(productAddressList.get(pos).getLatitude());
												double lon = Double.parseDouble(productAddressList.get(pos).getLongitude());
												/*String uri = String
														.format("geo:" + lat
																+ "," + lon
																+ "?z=20");
												Intent intent = new Intent(
														Intent.ACTION_VIEW, Uri
																.parse(uri));
												context.startActivity(intent);*/
												
												//String label = "HARI";
												String label = getAddress(lat,lon);
												
												String uriBegin = "geo:"+lat+","+lon;
												String query = lat+","+lon+"(" + label + ")";
												String encodedQuery = Uri.encode( query  );
												String uriString = uriBegin + "?q=" + encodedQuery;
												Uri uris = Uri.parse( uriString );
												
												Intent intent = new Intent(Intent.ACTION_VIEW, uris);
												context.startActivity(intent);
											} else {
												showDialog(NO_ADDRESS);
											}

										} catch (Exception e) {
											if (e != null) {
												e.printStackTrace();
												Log.w("HARI-->DEBUG", e);
											}

										}
									}
								});

						isAddressList = true;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void productLogosImagesLoading(String productLogoRCouponLogo2) {
		try {
			String productImageUrl = null;

			if(product.getDisplay_image().equals("Merchant Logo"))
			{
				productImageUrl = ApplicationConstants.PRODUCT_LOGO_WRAPPER
						+ product.getMerchant_id() + "."
						+ product.getLogo_extension();
			}
			else if(product.getDisplay_image().equals("Product Image"))
			{
				productImageUrl = ApplicationConstants.PRODUCT_IMAGE_LOGO_WRAPPER
						+ product.getId() + "." + product.getImage_extension();
				
				if(product.getImage_extension()==null)
				{
					productImageUrl = ApplicationConstants.PRODUCT_LOGO_WRAPPER
							+ product.getMerchant_id() + "."
							+ product.getLogo_extension();
				}
				
			}
			
			/*
			if (product.getImage_extension() == null) {

				productImageUrl = ApplicationConstants.PRODUCT_LOGO_WRAPPER
						+ product.getMerchant_id() + "."
						+ product.getLogo_extension();

			} else {
				productImageUrl = ApplicationConstants.PRODUCT_IMAGE_LOGO_WRAPPER
						+ product.getId() + "." + product.getImage_extension();
			}*/
			if (productImageUrl != null) {
				productImagesLoading(productImageUrl, productLogoRCouponLogo2);
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}
	
	private void productImagesLoading(String productImageUrl2, String productLogoRCouponLogo2) {
		// Loader image - will be shown before loading image
		SmartImageView logoIV = null;
		ProgressBar load1 = (ProgressBar) findViewById(R.id.progressloadID);
		if (productLogoRCouponLogo2.equals("productLogo")) {
			// this is for hot offers product logo
			logoIV = (SmartImageView) findViewById(R.id.productLogoIVID);
			logoIV.getLayoutParams().width = (int) (Utility.screenWidth / 2);
			logoIV.getLayoutParams().height = (int) (Utility.screenHeight / 5);
		} else if (productLogoRCouponLogo2.equals("couponLogo")) {
			// this is for coupon image instead of no image ImageView
			logoIV = (SmartImageView) findViewById(R.id.noImageID);
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					(int) (Utility.screenWidth / 3.0),
					(int) (Utility.screenHeight / 7.0));
			params.setMargins(0, Utility.screenWidth / 12, 0, 0);
			params.addRule(RelativeLayout.CENTER_HORIZONTAL);
			logoIV.setLayoutParams(params);
		}

		// Image url
		String image_url = productImageUrl2;
		Log.w("Hari-->", productImageUrl2);

		// ImageLoader class instance
	//	MyImageLoader imgLoader = new MyImageLoader(getApplicationContext(), productLogoRCouponLogo2);

		// whenever you want to load an image from url
		// call DisplayImage function
		// url - image url to load
		// loader - loader image, will be displayed before getting image
		// image - ImageView
		// imgLoader.DisplayImage(image_url, loader, bannerIV);
		try {
			//  imgLoader.DisplayImage(image_url, logoIV);
			logoIV.setImageUrl(image_url);
			} catch (OutOfMemoryError e) {
				if ( e != null) {
					Log.w("Hari-->DEBUG", e);
			}
		}

		if (logoIV != null) {
			load1.setVisibility(View.INVISIBLE);
		}
	}
	private String getAddress(double latitude, double longitude) {
		StringBuilder result = new StringBuilder();
		try {
			Geocoder geocoder = new Geocoder(this, Locale.getDefault());
			List<Address> addresses = geocoder.getFromLocation(latitude,
					longitude, 1);
			if (addresses.size() > 0) {
				Address address = addresses.get(0);
				result.append(address.getLocality()).append("\n");
				result.append(address.getCountryName());
			}
		} catch (IOException e) {
			Log.e("tag", e.getMessage());
		}

		return result.toString();
	}
	private void applyMenuListSlideAnimation() {
		TranslateAnimation translate = new TranslateAnimation(
				TranslateAnimation.RELATIVE_TO_SELF, 0,
				TranslateAnimation.RELATIVE_TO_SELF, 0,
				TranslateAnimation.RELATIVE_TO_SELF, -1,
				TranslateAnimation.RELATIVE_TO_SELF, 0);
		translate.setDuration(1000);
		LinearLayout detailsScrollLL = (LinearLayout) findViewById(R.id.detailsScrollLLID);
		detailsScrollLL.startAnimation(translate);
	}

	@SuppressWarnings("unused")
	@Override
	public void onClick(View view) {
		super.onClick(view);
		ScrollView detailsSV = (ScrollView) findViewById(R.id.productDescScrollViewID);
		switch (view.getId()) {
		case R.id.moneyDetailsBtnID:
			try {
				if (product != null) {
					if (product.getMobile_Reward() == 0) {
						showDialog(15);
					} else {
						if (product.getTerms_and_conditions() == null) {
							t_cBtn.setVisibility(View.GONE);
						}
						enableDisableButtons("disable");
						RelativeLayout coupondescription = (RelativeLayout) findViewById(R.id.coupondescriptionID);
						// favoritesRL.setEnabled(false);
						DisplayMetrics displaymetrics = new DisplayMetrics();
						getWindowManager().getDefaultDisplay().getMetrics(
								displaymetrics);
						int screenWidth = displaymetrics.widthPixels;
						coupondescription.getLayoutParams().width = (int) (screenWidth / 1.3);
						TextView prodTitleTV = (TextView) findViewById(R.id.prodTitleTVID);
						prodTitleTV.setSelected(true);
						prodTitleTV.setTypeface(Utility.font_bold);
						if(product.getName()!=null)
						{
						prodTitleTV.setText(product.getName());
						}
						
						productLogoRCouponLogo = "couponLogo";

						// product image loading....
						productLogosImagesLoading(productLogoRCouponLogo);

						// set offer
						TextView offerTV = (TextView) findViewById(R.id.highlightTVID);
						offerTV.setTypeface(Utility.font_bold);
						RelativeLayout.LayoutParams params6 = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
						params6.setMargins((int) (Utility.screenWidth / 60),
								(int) (Utility.screenWidth / 60),
								(int) (Utility.screenWidth / 60.0),
								(int) (Utility.screenWidth / 60.0));
						params6.addRule(RelativeLayout.CENTER_HORIZONTAL);
						params6.addRule(Gravity.CENTER_HORIZONTAL);
						offerTV.setLayoutParams(params6);
						if(product.getHighlight()!=null)
						{
						offerTV.setText(Html.fromHtml(product.getHighlight()));
						}
						// set user name
						TextView username = (TextView) findViewById(R.id.usernameTVID);
						username.setTypeface(Utility.font_reg);
						RelativeLayout.LayoutParams params7 = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
						params7.setMargins((int) (Utility.screenWidth / 60),
								(int) (Utility.screenWidth / 60),
								(int) (Utility.screenWidth / 60.0),
								(int) (Utility.screenWidth / 60.0));
						params7.addRule(RelativeLayout.CENTER_HORIZONTAL);
						params7.addRule(Gravity.CENTER_HORIZONTAL);
						params7.addRule(RelativeLayout.BELOW, R.id.highlightTVID);
						username.setLayoutParams(params7);
						username.setText("Name : " + Utility.user.getFirst_name() + " "
								+ Utility.user.getLast_name());

						// set membership number.....
						TextView mNo = (TextView) findViewById(R.id.mNoTVID);
						mNo.setTypeface(Utility.font_reg);
						RelativeLayout.LayoutParams params8 = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
						params8.setMargins((int) (Utility.screenWidth / 60),
								(int) (Utility.screenWidth / 60),
								(int) (Utility.screenWidth / 60.0),
								(int) (Utility.screenWidth / 60.0));
						params8.addRule(RelativeLayout.CENTER_HORIZONTAL);
						params8.addRule(Gravity.CENTER_HORIZONTAL);
						params8.addRule(RelativeLayout.BELOW, R.id.usernameTVID);
						mNo.setLayoutParams(params8);
						mNo.setText("MemberShip : " + Utility.user.getUsername());

						// set client name like TWU
						TextView clientName = (TextView) findViewById(R.id.clientNameTVID);
						clientName.setTypeface(Utility.font_reg);
						RelativeLayout.LayoutParams params9 = new RelativeLayout.LayoutParams(
								LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
						params9.setMargins((int) (Utility.screenWidth / 60),
								(int) (Utility.screenWidth / 60),
								(int) (Utility.screenWidth / 60.0),
								(int) (Utility.screenWidth / 60.0));
						params9.addRule(RelativeLayout.CENTER_HORIZONTAL);
						params9.addRule(Gravity.CENTER_HORIZONTAL);
						params9.addRule(RelativeLayout.BELOW, R.id.mNoTVID);
						clientName.setLayoutParams(params9);
						clientName.setText("Client Name : "
								+ Utility.user.getClient_name());
						offerDetailsRL.setVisibility(RelativeLayout.VISIBLE);

						Animation animBlink = AnimationUtils.loadAnimation(
								getApplicationContext(), R.anim.blink);
						// set animation listener
						// animBlink.start();

						RelativeLayout infoOnlyRL = (RelativeLayout) findViewById(R.id.infoOnlyRLID);
						infoOnlyRL.getLayoutParams().width = (int) (Utility.screenWidth / 1.3);
						infoOnlyRL.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);

						TextView infoOnlyOneTV = (TextView) findViewById(R.id.infoOnly1TVID);
						infoOnlyOneTV.setTypeface(Utility.font_bold);
						// infoOnlyOneTV.setTextSize(infoOnlyOneTV.getTextSize()+1);
						infoOnlyOneTV.setText("This is NOT a coupon offer.");
						// Info Only
						TextView infoOnlyTV = (TextView) findViewById(R.id.infoOnlyTVID);
						infoOnlyTV.setTypeface(Utility.font_reg);
						// infoOnlyTV.setTextSize(infoOnlyTV.getTextSize()+1);
						infoOnlyTV
								.setText("Refer to offer terms for redemption instructions..");

						if (product.getMobile_Reward() == 0) {
							redeemBtn.setVisibility(View.GONE);
							redeemBtn2.setVisibility(View.GONE);
							infoOnlyRL.setVisibility(View.GONE);
							infoOnlyTV.setVisibility(View.GONE);
							infoOnlyOneTV.setVisibility(View.GONE);
							// start the animation
							// infoOnlyRL.startAnimation(animBlink);
						} else {
							infoOnlyRL.setVisibility(View.GONE);
							infoOnlyTV.setVisibility(View.GONE);
							infoOnlyOneTV.setVisibility(View.GONE);
							redeemBtn.setVisibility(View.VISIBLE);
							redeemBtn2.setVisibility(View.VISIBLE);
						}
						offerDetailsRL.setVisibility(RelativeLayout.VISIBLE);
					}
				}
			} catch (Exception e) {
				enableDisableButtons("enable");
				if (e != null) {
					Log.w("HARI-->Debug", e);
					e.printStackTrace();
				}
			}
			break;
		case R.id.mapDetailsBtnID:
			try {
				isAddressList = true;
				TreeSet<String> mSeparatorsSet = new TreeSet<String>();
				detailsSV.setVisibility(View.GONE);
				/*
				 * detailsTV.setVisibility(View.GONE);
				 * textTV.setVisibility(View.GONE); t_cTV.setVisibility(View.GONE);
				 */
				addressContactsListView.setVisibility(ListView.VISIBLE);
				addressContactsListView.setDividerHeight(1);
				productAddressList = new ArrayList<ProductAddress>();

				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					GrabItNowService.getGrabItNowService()
							.sendProductAddresseRequest(this, productId);
				} else {
					loading.setVisibility(View.GONE);
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater.inflate(R.layout.toast_no_netowrk,
							(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
					// startActivity(new Intent(MyAccountActivity.this,
					// DashboardScreenActivity.class));
					ProductDetailsActivity.this.finish();
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("HARI-->Debug", e);
					e.printStackTrace();
				}
			}

			break;
		case R.id.contactDetailsBtnID:
			try {
				isAddressList = false;
				detailsSV.setVisibility(View.GONE);

				if (product != null) {
					getContactsList();
				}
				mAdapter = new MyAddressAdapter(this, R.layout.contacts_list_item);
				addressContactsListView.setAdapter(mAdapter);
				if (view.getId() == R.id.contactDetailsBtnID) {
					addressContactsListView.setDividerHeight(0);
				}
				addressContactsListView.setVisibility(ListView.VISIBLE);
				addressContactsListView
						.setOnItemClickListener(new OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> arg0,
									View rowView, final int pos, long arg3) {
								if (!isAddressList) {
									try {
										if (contactsList.get(pos).contains("www") || contactsList.get(pos).contains("http")) {
											String url = null;
											if (!contactsList.get(pos).startsWith("http://") && !contactsList.get(pos).startsWith("https://")){
												url = "http://"+ contactsList.get(pos);
											}
											else{
												url = contactsList.get(pos);
											}
											Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
											startActivity(browserIntent);
										} else if (contactsList.get(pos).startsWith("mailto:") || contactsList.get(pos).startsWith("@") || contactsList.get(pos).contains("@")) {
											if(contactsList.get(pos).startsWith("mailto:"))
											{
						                    Uri uri = Uri.parse(contactsList.get(pos));
						                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
						                    startActivity(intent);
											}
											else
											{
												 Uri uri = Uri.parse("mailto:"+contactsList.get(pos));
								                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
								                    startActivity(intent);
											}
						                } else {
						                	position=pos;
						                	showDialog(CALL_NUMBER);
						                }	
									} catch (NullPointerException e) {
										if (e != null) {
											Log.w("Hari-->DEBUG", e);
										}
									} catch (Exception e) {
										if (e != null) {
											Log.w("Hari-->DEBUG", e);
										}
									}																									
								}
							}
						});
			} catch (Exception e) {
				if (e != null) {
					Log.w("HARI-->Debug", e);
					e.printStackTrace();
				}
			}
			break;
		case R.id.saleDetailsBtnID:
			if (product != null) {
				detailsSV.setVisibility(View.VISIBLE);
				addressContactsListView.setVisibility(ListView.GONE);
				String temp = product.getDetails() + product.getText()
						+ product.getTerms_and_conditions();
			}

			break;
		case R.id.markFavouriteRLID:
			// ------Hari----- ///

			try {
				if (isProductStored) {
					showDialog(FAVOURITE_REMOVE_BUTTON);
				} else {
					showDialog(FAVOURITE_ADD_BUTTON);
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("HARI-->Debug", e);
					e.printStackTrace();
				}
			}
			break;
		case R.id.tcBtnID:
			if (product != null) {
				if (product.getTerms_and_conditions() != null) {
					tcDetailsTV.setTypeface(Utility.font_reg);
					tcDetailsTV.setText(Html.fromHtml(product.getTerms_and_conditions()));
					tcDetailsRL.setVisibility(RelativeLayout.VISIBLE);
				}
			}
			break;
		case R.id.redeemBtnID:
			try {
				showDialog(REDEEM_BUTTON);
			} catch (Exception e) {
				if (e != null) {
					Log.w("HARI-->Debug", e);
					e.printStackTrace();
				}
			}
			break;
		case R.id.redeemBtnID2:
			try {
				showDialog(REDEEM_BUTTON);
			} catch (Exception e) {
				if (e != null) {
					Log.w("HARI-->Debug", e);
					e.printStackTrace();
				}
			}
			break;
		case R.id.tc_backBtnID:
			tcDetailsRL.setVisibility(RelativeLayout.GONE);
			break;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		try {

			if (id == 1) {
				AlertDialog dialogRedeem = null;
				switch (id) {
				case REDEEM_BUTTON:
					LayoutInflater li = LayoutInflater.from(this);
					View redeemDialogView = li.inflate(
							R.layout.dialog_layout_redeem, null);
					AlertDialog.Builder adbr = new AlertDialog.Builder(this);
					adbr.setCancelable(false);
					adbr.setView(redeemDialogView);
					dialogRedeem = adbr.create();

					break;
				}
				return dialogRedeem;
			} else if (id == 2) {
				AlertDialog redeemDetailsYes = null;
				switch (id) {
				case REDEEM_YES_BUTTON:
					LayoutInflater liYes = LayoutInflater.from(this);
					View rdmYesView = liYes.inflate(
							R.layout.dialog_layout_redeem_yes, null);
					AlertDialog.Builder adbrYes = new AlertDialog.Builder(this);
					adbrYes.setCancelable(false);
					adbrYes.setView(rdmYesView);
					redeemDetailsYes = adbrYes.create();
					break;
				}
				return redeemDetailsYes;
			} else if (id == 3) {
				AlertDialog addFavorites = null;
				switch (id) {
				case FAVOURITE_ADD_BUTTON:
					LayoutInflater liadd = LayoutInflater.from(this);
					View addFav = liadd.inflate(
							R.layout.dialog_layout_add_favorites, null);
					AlertDialog.Builder adbAddFav = new AlertDialog.Builder(this);
					adbAddFav.setCancelable(false);
					adbAddFav.setView(addFav);
					addFavorites = adbAddFav.create();
					break;
				}
				return addFavorites;
			} else if (id == 4) {
				AlertDialog deleteFavorites = null;
				switch (id) {
				case FAVOURITE_REMOVE_BUTTON:
					LayoutInflater liDelete = LayoutInflater.from(this);
					View deleteFavView = liDelete.inflate(
							R.layout.dialog_layout_delete_favorite, null);
					AlertDialog.Builder adbDeleteFav = new AlertDialog.Builder(this);
					adbDeleteFav.setCancelable(false);
					adbDeleteFav.setView(deleteFavView);
					deleteFavorites = adbDeleteFav.create();
					break;
				}
				return deleteFavorites;
			} else if (id == 5) {
				AlertDialog noAddressdialog = null;
				switch (id) {
				case NO_ADDRESS:
					LayoutInflater liYes = LayoutInflater.from(this);
					View noAddressView = liYes.inflate(
							R.layout.dialog_layout_no_address_map, null);
					AlertDialog.Builder adbrok = new AlertDialog.Builder(this);
					adbrok.setCancelable(false);
					adbrok.setView(noAddressView);
					noAddressdialog = adbrok.create();
					break;
				}
				return noAddressdialog;
			} else if (id == 6) {
				AlertDialog callMobiledialog = null;
				switch (id) {
				case CALL_NUMBER:
					LayoutInflater liYes = LayoutInflater.from(this);
					View callAddressView = liYes.inflate(
							R.layout.dialog_layout_call_one, null);
					AlertDialog.Builder adbrok = new AlertDialog.Builder(this);
					adbrok.setCancelable(false);
					adbrok.setView(callAddressView);
					callMobiledialog = adbrok.create();
					break;
				}
				return callMobiledialog;
			} else if (id == 15) {
				AlertDialog noFavalert = null;

				LayoutInflater in1 = LayoutInflater.from(this);
				View dview = in1.inflate(R.layout.dialog_layout_no_redeem, null);
				AlertDialog.Builder dbl1 = new AlertDialog.Builder(this);
				dbl1.setCancelable(false);
				dbl1.setView(dview);
				noFavalert = dbl1.create();
				return noFavalert;
			}
		
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
			return null;
		}
		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		try {

			if (id == 1) {
				switch (id) {
				case REDEEM_BUTTON:
					final AlertDialog alertDialog1 = (AlertDialog) dialog;
					alertTitleTV = (TextView) alertDialog1
							.findViewById(R.id.alertLogoutTitleTVID);
					alertTitleTV.setTypeface(Utility.font_bold);
					Button yesButton = (Button) alertDialog1
							.findViewById(R.id.yesBtnID);
					yesButton.setTypeface(Utility.font_bold);
					Button noButton = (Button) alertDialog1
							.findViewById(R.id.noBtnID);
					noButton.setTypeface(Utility.font_bold);
					text1 = (TextView) alertDialog1
							.findViewById(R.id.redeemText1TVID);
					text1.setTypeface(Utility.font_bold);
					text2 = (TextView) alertDialog1
							.findViewById(R.id.redeemText2TVID);
					text2.setTypeface(Utility.font_reg);
					text3 = (TextView) alertDialog1
							.findViewById(R.id.redeemText3TVID);
					text3.setTypeface(Utility.font_bold);
					alertDialog1.setCancelable(false);

					yesButton.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {

							if (Utility
									.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
								GrabItNowService.getGrabItNowService()
										.sendRedeemDetailsRequest(
												ProductDetailsActivity.this,
												product.getId());
							} else {
								loading.setVisibility(View.GONE);
								// The Custom Toast Layout Imported here
								LayoutInflater inflater = getLayoutInflater();
								View layout = inflater
										.inflate(
												R.layout.toast_no_netowrk,
												(ViewGroup) findViewById(R.id.custom_toast_layout_id));

								// The actual toast generated here.
								Toast toast = new Toast(getApplicationContext());
								toast.setDuration(Toast.LENGTH_LONG);
								toast.setView(layout);
								toast.show();
								// startActivity(new Intent(MyAccountActivity.this,
								// DashboardScreenActivity.class));
								ProductDetailsActivity.this.finish();
							}

							alertDialog1.dismiss();
						}
					});

					noButton.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							alertDialog1.dismiss();
						}
					});
					break;
				}

			} else if (id == 2) {
				switch (id) {
				case REDEEM_YES_BUTTON:
					final AlertDialog alt1 = (AlertDialog) dialog;
					alertTitleTV = (TextView) alt1
							.findViewById(R.id.alertLogoutTitleTVID);
					alertTitleTV.setTypeface(Utility.font_bold);
					tv11 = (TextView) alt1.findViewById(R.id.myRedeeMOfferTVID);
					tv11.setTypeface(Utility.font_reg);
					if(product.getName()!=null)
					{
					tv11.setText("For Redeeming this offer " + product.getName());
					}
					else
					{
						tv11.setText("For Redeeming this offer ");
					}
					rdmYes = (Button) alt1.findViewById(R.id.yesBtnID);
					rdmYes.setTypeface(Utility.font_bold);
					alt1.setCancelable(false);
					rdmYes.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							alt1.dismiss();
							enableDisableButtons("enable");
							offerDetailsRL.setVisibility(RelativeLayout.GONE);
							tcDetailsRL.setVisibility(RelativeLayout.GONE);
						}
					});
					break;
				}
			}

			// ---------------Favourites Custom Dialogs -------------- //
			else if (id == 3) {
				switch (id) {
				case FAVOURITE_ADD_BUTTON:
					final AlertDialog alt3 = (AlertDialog) dialog;
					alertTitleTV = (TextView) alt3
							.findViewById(R.id.firstLoginCEPUTitleTVID);
					alertTitleTV.setTypeface(Utility.font_bold);
					tv22 = (TextView) alt3.findViewById(R.id.addFavTVID);
					tv22.setTypeface(Utility.font_reg);
					if(productName!=null)
					{
					tv22.setText("Do you want to add" + " " + productName + " "
							+ "to favourites?");
					}
					else
					{
						tv22.setText("Do you want to add to favourites?");
					}
					addFavYesBtn = (Button) alt3
							.findViewById(R.id.add_fav_yesBtnID);
					addFavYesBtn.setTypeface(Utility.font_bold);
					addNoFavBtn = (Button) alt3.findViewById(R.id.add_fav_noBtnID);
					addNoFavBtn.setTypeface(Utility.font_bold);
					alt3.setCancelable(false);
					addFavYesBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							dbHelper.addFavoriteProductDetails(
									Integer.toString(productId), productName,
									productHighlight);

							isProductStored = true;
							favoritesRL
									.setBackgroundResource(R.drawable.heart_full);
							alt3.dismiss();
						}
					});
					addNoFavBtn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							alt3.dismiss();
						}
					});
					break;
				}
			} else if (id == 4) {
				switch (id) {
				case FAVOURITE_REMOVE_BUTTON:
					final AlertDialog alt3 = (AlertDialog) dialog;
					alertTitleTV = (TextView) alt3
							.findViewById(R.id.firstLoginCEPUTitleTVID);
					alertTitleTV.setTypeface(Utility.font_bold);
					tv22 = (TextView) alt3.findViewById(R.id.deleteFavTVID);
					tv22.setTypeface(Utility.font_bold);
					if(productName!=null)
					{
					tv22.setText("Are you sure want to remove" + " " + productName
							+ " " + "from favourites?");
					}
					else
					{
						tv22.setText("Are you sure want to remove from favourites?");
					}
					addFavYesBtn = (Button) alt3
							.findViewById(R.id.delete_fav_yesBtnID);
					addFavYesBtn.setTypeface(Utility.font_bold);
					alt3.setCancelable(false);
					addNoFavBtn = (Button) alt3
							.findViewById(R.id.delete_fav_noBtnID);
					addNoFavBtn.setTypeface(Utility.font_bold);
					addFavYesBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							dbHelper.deleteProduct(Integer.toString(productId));
							isProductStored = false;
							favoritesRL
									.setBackgroundResource(R.drawable.heart_empty);
							alt3.dismiss();
						}
					});
					addNoFavBtn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							alt3.dismiss();
						}
					});
					break;
				}
			}
			else if (id == 5) {
				switch (id) {
				case NO_ADDRESS:
					final AlertDialog alt4 = (AlertDialog) dialog;
					alertTitleTV = (TextView) alt4
							.findViewById(R.id.firstLoginGINTitleTVID);
					alertTitleTV.setTypeface(Utility.font_bold);
					tv11 = (TextView) alt4.findViewById(R.id.noAddressTVID);
					tv11.setTypeface(Utility.font_reg);
					Button okBtnNoMap = (Button) alt4
							.findViewById(R.id.noAddressOkBtnID);
					okBtnNoMap.setTypeface(Utility.font_bold);
					alt4.setCancelable(false);
					okBtnNoMap.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							alt4.dismiss();
						}
					});
					break;
				}
			} else if (id == 6) {
				switch (id) {
				case CALL_NUMBER:
					final AlertDialog alt3 = (AlertDialog) dialog;
					alertTitleTV = (TextView) alt3.findViewById(R.id.firstLoginCEPUTitleTVID);
					alertTitleTV.setTypeface(Utility.font_bold);
					tv22 = (TextView) alt3.findViewById(R.id.callNumberOneTVID);
					tv22.setTypeface(Utility.font_reg);
					tv22.setText(contactsList.get(position));
					addFavYesBtn = (Button) alt3.findViewById(R.id.call_one_yesBtnID);
					addFavYesBtn.setTypeface(Utility.font_bold);
					addNoFavBtn = (Button) alt3.findViewById(R.id.call_one_noBtnID);
					if (contactsList.get(position).equals("No contact number available")) {
						addNoFavBtn.setVisibility(View.GONE);
						addFavYesBtn.setText(getResources().getString(R.string.ok_one));
					}
					addNoFavBtn.setTypeface(Utility.font_bold);
					addFavYesBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							try {
								if (!contactsList.get(position).equals(
										"No contact number available")) {
									Intent callIntent = new Intent(
											Intent.ACTION_CALL);
									callIntent.setData(Uri.parse("tel:"
											+ contactsList.get(position)));
									startActivity(callIntent);
								}
							} catch (ActivityNotFoundException e) {
								e.printStackTrace();
							}

							alt3.dismiss();
						}
					});
					addNoFavBtn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							alt3.dismiss();
						}
					});
					break;
				}
			} else if (id == 15) {
				final AlertDialog noFavalert = (AlertDialog) dialog;

				TextView alertTitle = (TextView) noFavalert
						.findViewById(R.id.firstLoginGINTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);

				TextView noFavTV = (TextView) noFavalert
						.findViewById(R.id.nofavTVID);

				noFavTV.setTypeface(Utility.font_reg);

				Button noFavOKBtn = (Button) noFavalert
						.findViewById(R.id.noFavOkBtnID);
				noFavOKBtn.setTypeface(Utility.font_bold);
				noFavalert.setCancelable(false);

				noFavOKBtn.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						noFavalert.dismiss();
					}
				});
			}
			return;
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
	}

	private void showRedeemThanxDialog() {
		showDialog(REDEEM_YES_BUTTON);
	}

	private void getContactsList() {
		try {
			contactsList = new ArrayList<String>();
			if (product != null) {
				if (product.getPhone() != null) {
					
					String[] numbersArray=product.getPhone().split(",");
					
					if(numbersArray!=null)
					{
					
					if(numbersArray.length>1)
					{
						for(int i=0;i<numbersArray.length;i++)
						{
							String mobiNo=numbersArray[i];
							if(mobiNo!=null)
							{
							if (mobiNo.contains("/")) {
								String contact1 =mobiNo.split("/")[0].trim();
								String contact2 =mobiNo.split("/")[1].trim();
								if(contact2!=null && contact2.length()>1)
								{
								contact2 = contact1.substring(0,
										contact1.length() - contact2.length() - 1)
										+ contact2;
								contactsList.add(contact1);
								contactsList.add(contact2);
								}
								else
								{
									contactsList.add(contact1);
									
								}
							} else {
								contactsList.add(mobiNo.trim());
							}
							}
						}
					}
					else
					{
						if (product.getPhone().contains("/")) {
							String contact1 = product.getPhone().split("/")[0].trim();
							String contact2 = product.getPhone().split("/")[1].trim();
							contact2 = contact1.substring(0,
									contact1.length() - contact2.length())
									+ contact2;
							contactsList.add(contact1);
							contactsList.add(contact2);
						} else {
							contactsList.add(product.getPhone().trim());
						}
					}
					}
					/*if (product.getPhone().contains("/")) {
						String contact1 = product.getPhone().split("/")[0].trim();
						String contact2 = product.getPhone().split("/")[1].trim();
						contact2 = contact1.substring(0,
								contact1.length() - contact2.length() - 1)
								+ contact2;
						contactsList.add(contact1);
						contactsList.add(contact2);
					} else {
						contactsList.add(product.getPhone().trim());
					}*/
				} else {
					contactsList.add("No contact number available");
				}
				if (product.getLink1() != null) {
					contactsList.add(product.getLink1());
				}
				if (product.getLink2() != null) {
					contactsList.add(product.getLink2());
				}
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}

	public class MyAddressAdapter extends BaseAdapter {
		LayoutInflater inflater;
		ProductDetails product;
		int layout;
		ProductDetailsActivity context;

		public MyAddressAdapter(ProductDetailsActivity context, int listItemLayout) {
			this.layout = listItemLayout;
			this.context = context;
		}

		@Override
		public int getCount() {
			if (layout == R.layout.contacts_list_item) {
				return contactsList.size();
			} else {
				return productAddressList.size();
			}
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int pos, View rowView, ViewGroup arg2) {
			if (rowView == null) {
				inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				rowView = (View) inflater.inflate(layout, null, false);
			}
			if (layout == R.layout.contacts_list_item) {
				TextView contactTV = (TextView) rowView
						.findViewById(R.id.contactTVID);
				contactTV.setTypeface(Utility.font_reg);
				ImageView contactIV = (ImageView) rowView
						.findViewById(R.id.contactIVID);
				contactIV.getLayoutParams().width = (int) (Utility.screenWidth / 12.0);
				contactIV.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
				contactTV.setText(contactsList.get(pos));
				if (contactsList.get(pos).contains("www") || contactsList.get(pos).contains("http")) {
					contactIV.setBackgroundResource(R.drawable.web);
				} else if (contactsList.get(pos).startsWith("mailto:") || contactsList.get(pos).startsWith("@") || contactsList.get(pos).contains("@")) {
					contactIV.setBackgroundResource(R.drawable.email_icon);
                } else {
                	contactIV.setBackgroundResource(R.drawable.call);
                }
			} else {
				addressTV = (TextView) rowView.findViewById(R.id.addressTVID);
				addressTV.setTypeface(Utility.font_reg);
				if (productAddressList.size() == 1) {
					if (productAddressList.get(pos).getAddress1() != null) {
						if (productAddressList.get(pos).getAddress1()
								.toString().trim().length() > 0) {
							String str[] = {
									productAddressList.get(pos).getAddress1(),
									productAddressList.get(pos).getSuburb(),
									productAddressList.get(pos).getCity(),
									productAddressList.get(pos).getState(),
									productAddressList.get(pos).getPostcode(),
									productAddressList.get(pos).getPhone() };
							String s = "";
							StringBuffer buf = new StringBuffer(s);
							for (int i = 0; i < 6; i++) {
								if (str[i] != null && str[i] != ""
										&& str[i] != "\0") {

									if (i == 0) {
										buf.append(str[i]).append("\n");
									} else if (i == 1) {
										buf.append(str[i]).append(",");
									} else if (i == 2) {
										buf.append(str[i]).append(",");

									} else if (i == 3) {
										buf.append(str[i]).append(",")
												.append("\n");
									} else if (i == 4) {
										buf.append("Pin Code-" + str[i]);
									} else if (i == 5) {
										buf.append(",").append("Ph-" + str[i]);
									}

									/*
									 * if (i == 0 || i == 1)
									 * buf.append(str[i]).append(","); else if
									 * (i == 2) buf.append("\n").append(str[i]);
									 * else if (i == 3)
									 * buf.append(",").append(str[i]); else if
									 * (i == 4) buf.append("\n").append(str[i]);
									 * else if (i == 5)
									 * buf.append(",").append(str[i]);
									 */

								}
							}
							addressTV.setText(buf);
						} else {
							mapViewIV = (ImageView) rowView
									.findViewById(R.id.mapAddressIVID);
							mapTextNameTV = (TextView) rowView
									.findViewById(R.id.mapTextNameTVID);
							addressTV.setText("No Address Available Here:");
							mapViewIV.setVisibility(View.GONE);
							mapTextNameTV.setVisibility(View.GONE);
						}
					} else {
						mapViewIV = (ImageView) rowView
								.findViewById(R.id.mapAddressIVID);
						mapTextNameTV = (TextView) rowView
								.findViewById(R.id.mapTextNameTVID);
						addressTV.setText("No Address Available Here:");
						mapViewIV.setVisibility(View.GONE);
						mapTextNameTV.setVisibility(View.GONE);
					}
				} else {
					String str[] = { productAddressList.get(pos).getAddress1(),
							productAddressList.get(pos).getSuburb(),
							productAddressList.get(pos).getCity(),
							productAddressList.get(pos).getState(),
							productAddressList.get(pos).getPostcode(),
							productAddressList.get(pos).getPhone() };
					String s = "";
					StringBuffer buf = new StringBuffer(s);
					for (int i = 0; i < 6; i++) {
						if (str[i] != null && str[i] != "" && str[i] != "\0") {

							if (i == 0) {
								buf.append(str[i]).append("\n");
							} else if (i == 1) {
								buf.append(str[i]).append(",");
							} else if (i == 2) {
								buf.append(str[i]).append(",");

							} else if (i == 3) {
								buf.append(str[i]).append(",").append("\n");
							} else if (i == 4) {
								buf.append("Pin Code-" + str[i]);
							} else if (i == 5) {
								buf.append(",").append("Ph-" + str[i]);
							}

							/*
							 * if (i == 0 || i == 1)
							 * buf.append(str[i]).append(","); else if (i == 2)
							 * buf.append("\n").append(str[i]); else if (i == 3)
							 * buf.append(",").append(str[i]); else if (i == 4)
							 * buf.append("\n").append(str[i]); else if (i == 5)
							 * buf.append(",").append(str[i]);
							 */

						}
					}
					addressTV.setText(buf);
				}
			}
			return rowView;
		}
	}
	public static Intent hariEmailIntent(Context context, String address,
			String subject, String body, String cc) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
		intent.putExtra(Intent.EXTRA_TEXT, body);
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_CC, cc);
		intent.setType("message/rfc822");
		return intent;
	}
}
