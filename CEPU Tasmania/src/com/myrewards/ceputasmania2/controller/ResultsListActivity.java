package com.myrewards.ceputasmania2.controller;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.ceputasmania2.model.Product;
import com.myrewards.ceputasmania2.service.CEPUServiceListener;
import com.myrewards.ceputasmania2.service.GrabItNowService;
import com.myrewards.ceputasmania2.utils.ApplicationConstants;
import com.myrewards.ceputasmania2.utils.Utility;

public class ResultsListActivity extends BaseActivity implements
		OnItemClickListener, CEPUServiceListener, OnScrollListener,
		OnClickListener {
	List<Product> productsList;
	List<Product> productsListTemp;
	ResultAdapter mAdapter;
	LayoutInflater inflater;
	View mLoading;
	ListView resultsListView;
	String catID = null;
	String location = null;
	String keyword = null;
	private int mvisibleItemCount = -1;
	private String fetchDirectionUP = Utility.FETCH_DIRECTION_UP;
	private String fetchDirection = "";
	private int visibleThreshold = 0;
	private int previousTotal = 0;
	private boolean loading = true;
	private int mfirstVisibleItem;;

	int productsCount = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.results_list);
		setHeaderTitle(getResources().getString(R.string.search_products_text));
		showBackButton();
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();

		// setHeaderFooterDimentions();
		Button backBtnHeader = (Button) findViewById(R.id.backBtnID);
		backBtnHeader.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtnHeader.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		Button scanBtn = (Button) findViewById(R.id.scanBtnID);
		scanBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		scanBtn.getLayoutParams().height = (int) (Utility.screenHeight / 13.5);
		scanBtn.setVisibility(View.VISIBLE);
		backBtnHeader.setOnClickListener(this);
		scanBtn.setOnClickListener(this);
		mLoading = (View) findViewById(R.id.loading);
		resultsListView = (ListView) findViewById(R.id.resultsListViewID);
		productsList = new ArrayList<Product>();
		productsListTemp = new ArrayList<Product>();
		resultsListView.setOnItemClickListener(this);
		resultsListView.setOnScrollListener(this);
		if (getIntent() != null) {
			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				catID = bundle.getString(ApplicationConstants.CAT_ID_KEY);
				location = bundle.getString(ApplicationConstants.LOCATION_KEY);
				keyword = bundle.getString(ApplicationConstants.KEYWORK_KEY);
			}

			if (Utility
					.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				if (catID != null) {
					GrabItNowService.getGrabItNowService()
							.sendSearchProductsRequest(this, catID, location,
									keyword, 0, 30);
				} else {
					GrabItNowService.getGrabItNowService()
							.sendHotOffersRequest(this);
				}
			} else {

				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
						(ViewGroup) findViewById(R.id.custom_toast_layout_id));

				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
				// startActivity(new Intent(MyAccountActivity.this,
				// DashboardScreenActivity.class));
				ResultsListActivity.this.finish();
			}

		}

	}

	public class ResultAdapter extends BaseAdapter {
		Context ctx;

		public ResultAdapter(ResultsListActivity resultsListActivity) {
			this.ctx = resultsListActivity;

		}

		@Override
		public int getCount() {
			return productsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {
			View resultsListRow = null;
			if (resultsListRow == null) {
				inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				resultsListRow = (View) inflater.inflate(
						R.layout.results_list_item, null, false);
			}
			LinearLayout rowLL = (LinearLayout) resultsListRow
					.findViewById(R.id.resultListItemLLID);
			LinearLayout rowLL2 = (LinearLayout) resultsListRow
					.findViewById(R.id.resultListItemHariLLID);
			rowLL2.getLayoutParams().height = (int) (Utility.screenHeight / 8.5);
			TextView productNameTV = (TextView) resultsListRow
					.findViewById(R.id.productTVID);
			productNameTV.setTypeface(Utility.font_bold);
			TextView highlightTV = (TextView) resultsListRow
					.findViewById(R.id.offerTVID);
			highlightTV.setTypeface(Utility.font_reg);
			productNameTV.setText(productsList.get(pos).getName());
			highlightTV.setText(productsList.get(pos).getHighlight());
			switch (pos % 2) {
			case 0:
				rowLL.setBackgroundResource(R.color.result_color_one);
				productNameTV.setTextColor(Color.parseColor("#2E52AC"));

				break;
			case 1:
				rowLL.setBackgroundResource(R.color.result_color_two);
				productNameTV.setTextColor(Color.parseColor("#60B3FF"));
				break;
			}
			Animation animation = AnimationUtils.loadAnimation(ctx,	R.anim.push_left_in);
			resultsListRow.startAnimation(animation);
			animation = null;
			return resultsListRow;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos,
			long arg3) {
		super.onItemClick(arg0, rowView, pos, arg3);
		Intent detailsIntent = new Intent(ResultsListActivity.this,
				ProductDetailsActivity.class);
		detailsIntent.putExtra(ApplicationConstants.PRODUCT_ID_KEY,
				productsList.get(pos).getId());
		detailsIntent.putExtra(ApplicationConstants.COLOR_CODE_KEY, pos % 2);
		detailsIntent.putExtra(ApplicationConstants.PRODUCT_NAME_KEY,
				productsList.get(pos).getName());
		detailsIntent.putExtra(ApplicationConstants.PRODUCT_HIGHLIGHT_KEY,
				productsList.get(pos).getHighlight());
		startActivity(detailsIntent);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (response != null) {
				if (response instanceof String) {
					// showErrorDialog(response.toString());
					mLoading.setVisibility(View.GONE);
					showDialog(1);

					// Utility.showMessage(this, response.toString());
				} else {
					if (eventType != 16) {
						productsList = (ArrayList<Product>) response;

						productsCount = productsCount + productsList.size();
						if (!(productsCount > 0)) {
							mLoading.setVisibility(View.GONE);
							showDialog(1);
						} else {
							mLoading.setVisibility(View.GONE);

							if (fetchDirection
									.equalsIgnoreCase(fetchDirectionUP)) {

								int position = productsListTemp.size();
								productsListTemp.addAll(productsList);
								productsList = productsListTemp;

								mAdapter = new ResultAdapter(this);
								resultsListView.setAdapter(mAdapter);
								resultsListView.setScrollingCacheEnabled(false);
								if (productsList.size() > 0
										&& mvisibleItemCount != -1)
									resultsListView.setSelection(position
											- mvisibleItemCount + 2);
								else
									resultsListView.setSelection(position);
							}

							if (mAdapter == null) {
								mAdapter = new ResultAdapter(this);
								resultsListView.setAdapter(mAdapter);
							} else {
								mAdapter.notifyDataSetChanged();
							}
						}
					}
				}

			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			if (id == 1) {
				LayoutInflater inflater = LayoutInflater.from(this);
				View noSearchResults = inflater.inflate(
						R.layout.dailog_layout_for_searchlist_noitem, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
				dialogbuilder.setCancelable(false);
				dialogbuilder.setView(noSearchResults);
				AlertDialog dialogDetails = dialogbuilder.create();
				return dialogDetails;
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		try {
			if (id == 1) {
				final AlertDialog alertDialog2 = (AlertDialog) dialog;
				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.alertTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);
				TextView tv15 = (TextView) alertDialog2
						.findViewById(R.id.loginFieldTVID);
				tv15.setTypeface(Utility.font_reg);
				Button okbutton = (Button) alertDialog2
						.findViewById(R.id.nosearchResultsID);
				okbutton.setTypeface(Utility.font_bold);
				alertDialog2.setCancelable(false);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
						finish();
					}
				});

			}
			super.onPrepareDialog(id, dialog);
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		try {
			mvisibleItemCount = visibleItemCount;
			if (firstVisibleItem > mfirstVisibleItem && !loading
					&& (firstVisibleItem + visibleItemCount == totalItemCount)
					&& totalItemCount > 0 && totalItemCount != visibleItemCount) {
				fetchDirection = fetchDirectionUP;
				System.out.println("firstVisibleItem" + firstVisibleItem);
				System.out.println("visibleItemCount" + visibleItemCount);
				System.out.println("totalItemCount" + totalItemCount);
				if (!loading
						&& (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)
						&& totalItemCount != 0
						&& mLoading.getVisibility() != View.VISIBLE) {
					sendRequestWithScrollDirection(fetchDirectionUP, totalItemCount);
					loading = true;

				}
			}

			if (loading) {
				if (totalItemCount > previousTotal) {
					loading = false;
					previousTotal = totalItemCount;

				}
			}
			mfirstVisibleItem = firstVisibleItem;
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
	}

	private void sendRequestWithScrollDirection(String DirectiontoFetch,
			int totalItemCount) {
		try {
			if (productsList.size() == totalItemCount) {
				mLoading.setVisibility(View.VISIBLE);
				fetchDirection = DirectiontoFetch;
				productsListTemp = productsList;
				if ((productsCount - 30) >= 0) {
					if (Utility
							.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
						GrabItNowService.getGrabItNowService()
								.sendSearchProductsRequest(this, catID, location,
										keyword, productsCount, 10);
					} else {

						// The Custom Toast Layout Imported here
						LayoutInflater inflater = getLayoutInflater();
						View layout = inflater
								.inflate(
										R.layout.toast_no_netowrk,
										(ViewGroup) findViewById(R.id.custom_toast_layout_id));

						// The actual toast generated here.
						Toast toast = new Toast(getApplicationContext());
						toast.setDuration(Toast.LENGTH_LONG);
						toast.setView(layout);
						toast.show();
						// startActivity(new Intent(MyAccountActivity.this,
						// DashboardScreenActivity.class));
						ResultsListActivity.this.finish();

					}
				} else
					mLoading.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		if (R.id.backBtnID == v.getId()) {
			finish();
		} else if (R.id.scanBtnID == v.getId()) {
			startActivity(new Intent(ResultsListActivity.this,
					BarCodeScannerActivity.class));
		}
	}
}
