package com.myrewards.ceputasmania2.controller;

import java.util.ArrayList;
import java.util.Collection;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.ceputasmania2.model.Category;
import com.myrewards.ceputasmania2.service.CEPUServiceListener;
import com.myrewards.ceputasmania2.service.GrabItNowService;
import com.myrewards.ceputasmania2.utils.ApplicationConstants;
import com.myrewards.ceputasmania2.utils.Utility;

@SuppressLint("ShowToast")
public class SearchListActivity extends BaseActivity implements
		OnItemClickListener, OnClickListener, CEPUServiceListener,
		OnLongClickListener {
	LayoutInflater inflater;
	SearchListAdapter mAdapter;
	ArrayList<Category> categoryList;
	ArrayList<Category> categoryListTemp;
	View loading;
	Button searchBtn, okbutton;
	ListView searchListView;
	Button resetBtn;
	EditText locationET;
	EditText keywordET;
	TextView category1;
	ImageView catImage;
	TextView searchAlertTV;
	RadioButton selectedRowRB;
	private static final int SEARCH_LIST_SELECTION = 1;

	// CheckBox check;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_list_main);

		// check=(CheckBox)findViewById(R.id.chechboxID);

		setHeaderTitle(getResources().getString(R.string.search_text));
		categoryList = new ArrayList<Category>();
		loading = (View) findViewById(R.id.loading);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		Button helpBtn;
		helpBtn = (Button) findViewById(R.id.helpBtnID);
		helpBtn.getLayoutParams().width = (int) (Utility.screenWidth / 5.6);
		helpBtn.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
		helpBtn.setOnClickListener(this);
		locationET = (EditText) findViewById(R.id.enterLocationETID);
		locationET.setTypeface(Utility.font_reg);
		locationET.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
		keywordET = (EditText) findViewById(R.id.enterKeywordETID);
		keywordET.setTypeface(Utility.font_reg);
		keywordET.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
		searchBtn = (Button) findViewById(R.id.searchBtnID);
		searchBtn.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
		searchBtn.setOnClickListener(this);
		resetBtn = (Button) findViewById(R.id.resetBtnID);
		resetBtn.getLayoutParams().width = (int) (Utility.screenWidth / 5.6);
		resetBtn.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
		resetBtn.setOnClickListener(this);
		searchListView = (ListView) findViewById(R.id.searchListViewID);
		searchListView.setOnItemClickListener(this);

		menuListView = (ListView) findViewById(R.id.menuListViewID);

		locationET.setOnLongClickListener(this);
		keywordET.setOnLongClickListener(this);

		initialiseViews();
		mAdapter = new SearchListAdapter(this);
		categoryList = new ArrayList<Category>();
		categoryListTemp = new ArrayList<Category>();

		

		if (Utility
				.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			GrabItNowService.getGrabItNowService().sendCategoriesListRequest(
					this);
		} else {
			loading.setVisibility(View.GONE);
			// The Custom Toast Layout Imported here
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));

			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();
			SearchListActivity.this.finish();
		}
	}

	public class SearchListAdapter extends BaseAdapter {
		Context ctx;

		public SearchListAdapter(SearchListActivity searchListActivity) {
			this.ctx = searchListActivity;

		}

		@Override
		public int getCount() {
			return categoryList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {
			View searchListRow = null;
			if (searchListRow == null) {
				inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				searchListRow = (ViewGroup) inflater.inflate(
						R.layout.search_list_item, null, false);
			}
			LinearLayout searchlistLL = (LinearLayout) searchListRow
					.findViewById(R.id.searchListsLLID);
			searchlistLL.getLayoutParams().height = (int) (Utility.screenHeight / 9.2);
			category1 = (TextView) searchListRow
					.findViewById(R.id.categoryTVID);
			category1.setTypeface(Utility.font_bold);
			catImage = (ImageView) searchListRow.findViewById(R.id.catImageID);
			category1.setText(categoryList.get(pos).getName());
			category1.setTextColor(getResources().getColor(R.color.handlife));
			String cat = categoryList.get(pos).getName();

			if (cat.toLowerCase().contains("automotive")) {
				catImage.setBackgroundResource(R.drawable.pin_type1);
			}

			else if (cat.toLowerCase().contains("dining")) {
				catImage.setBackgroundResource(R.drawable.pin_type2);
			} else if (cat.toLowerCase().contains("golf courses")) {
				catImage.setBackgroundResource(R.drawable.pin_type8);
			} else if (cat.toLowerCase().contains("golf handicaps")) {
				catImage.setBackgroundResource(R.drawable.pin_type8);
			} else if (cat.toLowerCase().contains("golf insurance")) {
				catImage.setBackgroundResource(R.drawable.pin_type9);
			} else if (cat.toLowerCase().contains("health")) {
				catImage.setBackgroundResource(R.drawable.pin_type3);
			} else if (cat.toLowerCase().contains("home")) {
				catImage.setBackgroundResource(R.drawable.pin_type4);
			} else if (cat.toLowerCase().contains("entertainment")) {
				catImage.setBackgroundResource(R.drawable.pin_type5);
			} else if (cat.toLowerCase().contains("shopping")) {
				catImage.setBackgroundResource(R.drawable.pin_type6);
			} else if (cat.toLowerCase().contains("travel")) {
				catImage.setBackgroundResource(R.drawable.pin_type7);
			} else {
				catImage.setBackgroundResource(R.drawable.pin_type10);
			}

			selectedRowRB = (RadioButton) searchListRow.findViewById(R.id.categoryRBID);
			if (categoryList.get(pos).isSelected()) {
				selectedRowRB.setChecked(true);
			} else {
				selectedRowRB.setChecked(false);
			}
			return searchListRow;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos,
			long arg3) {
		super.onItemClick(arg0, rowView, pos, arg3);
		if (menuListView.getVisibility() == ListView.GONE) {
			for (int i = 0; i < categoryList.size(); i++) {
				categoryList.get(i).setSelected(false);
			}
			categoryList.get(pos).setSelected(true);
			mAdapter.notifyDataSetChanged();
		}
	}

	@SuppressLint("ShowToast")
	@Override
	public void onClick(View view) {
		super.onClick(view);
		switch (view.getId()) {
		case R.id.helpBtnID:
			LayoutInflater inflater = getLayoutInflater();
			View helpDialogView = (View) inflater.inflate(R.layout.help_dialog,
					null, false);
			final Dialog messageDialog = new Dialog(this,
					android.R.style.Theme_Translucent_NoTitleBar); // old
			TextView windowTitle = (TextView) helpDialogView
					.findViewById(R.id.info_heading);
			windowTitle.setTypeface(Utility.font_reg);

			TextView helpDesc1TVID = (TextView) helpDialogView
					.findViewById(R.id.helpDesc1TVID);
			helpDesc1TVID.setTypeface(Utility.font_reg);

			TextView helpDesc2TVID = (TextView) helpDialogView
					.findViewById(R.id.helpDesc2TVID);
			helpDesc2TVID.setTypeface(Utility.font_reg);

			TextView helpDesc8TVID = (TextView) helpDialogView
					.findViewById(R.id.helpDesc8TVID);
			helpDesc8TVID.setTypeface(Utility.font_reg);

			TextView helpDesc3TVID = (TextView) helpDialogView
					.findViewById(R.id.helpDesc3TVID);
			helpDesc3TVID.setTypeface(Utility.font_reg);

			TextView helpDesc4TVID = (TextView) helpDialogView
					.findViewById(R.id.helpDesc4TVID);
			helpDesc4TVID.setTypeface(Utility.font_reg);

			TextView helpDesc5TVID = (TextView) helpDialogView
					.findViewById(R.id.helpDesc5TVID);
			helpDesc5TVID.setTypeface(Utility.font_reg);

			TextView helpDesc6TVID = (TextView) helpDialogView
					.findViewById(R.id.helpDesc6TVID);
			helpDesc6TVID.setTypeface(Utility.font_reg);

			TextView helpDesc7TVID = (TextView) helpDialogView
					.findViewById(R.id.helpDesc7TVID);
			helpDesc7TVID.setTypeface(Utility.font_reg);

			TextView helpDesc9TVID = (TextView) helpDialogView
					.findViewById(R.id.helpDesc9TVID);

			helpDesc9TVID.setText(getResources().getString(R.string.info_text9)
					+ "\n \n" + getResources().getString(R.string.info_text10)
					+ "\n \n" + getResources().getString(R.string.info_text11));

			helpDesc9TVID.setTypeface(Utility.font_reg);
			// Theme_Translucent_NoTitleBar
			// //
			Window window = messageDialog.getWindow();
			window.setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND,
					WindowManager.LayoutParams.FLAG_DIM_BEHIND);
			window.setLayout((int) (8 * Utility.screenWidth / 9.2),
					5 * Utility.screenHeight / 6);
			window.getAttributes().dimAmount = 0.7f;
			messageDialog.setCancelable(true);
			messageDialog.setContentView(helpDialogView);
			messageDialog.show();
			ImageView myhee = (ImageView) helpDialogView
					.findViewById(R.id.infoButtonIVID);
			myhee.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					messageDialog.cancel();
				}
			});
			break;
		case R.id.searchBtnID:
			String location = locationET.getText().toString();
			String keyword = keywordET.getText().toString();
			String catID = null;

			for (int i = 0; i < categoryList.size(); i++) {
				if (categoryList.get(i).isSelected()) {
					catID = Integer.toString(categoryList.get(i).getCatID());
					break;
				}
			}
			if (catID == null) {
				showDialog(SEARCH_LIST_SELECTION);
			} else {
				
				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent resultIntent = new Intent(SearchListActivity.this,
							ResultsListActivity.class);
					resultIntent.putExtra(ApplicationConstants.CAT_ID_KEY, catID);
					resultIntent.putExtra(ApplicationConstants.LOCATION_KEY,
							location.trim());
					resultIntent.putExtra(ApplicationConstants.KEYWORK_KEY,
							keyword.trim());
					startActivity(resultIntent);
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater2 = getLayoutInflater();
					View layout = inflater2.inflate(R.layout.toast_no_netowrk,
							(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
				}
			}

			break;
		case R.id.resetBtnID:
			keywordET.setText("");
			locationET.setText("");
			// check.setChecked(false);
			for (int i = 0; i < categoryList.size(); i++) {
				categoryList.get(i).setSelected(false);
			}
			mAdapter.notifyDataSetChanged();
			break;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			if (id == 1) {
				AlertDialog dialogDetails = null;
				LayoutInflater inflater = LayoutInflater.from(this);
				View dialogview = inflater.inflate(
						R.layout.dialog_layout_search_list_location, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
				dialogbuilder.setCancelable(false);
				dialogbuilder.setView(dialogview);
				dialogDetails = dialogbuilder.create();
				return dialogDetails;
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		super.onPrepareDialog(id, dialog);
		try {
			if (id == 1) {
				final AlertDialog alertDialog2 = (AlertDialog) dialog;
				TextView alerttitle = (TextView) alertDialog2
						.findViewById(R.id.alertLogoutTitleTVID);
				alerttitle.setTypeface(Utility.font_bold);
				searchAlertTV = (TextView) alertDialog2
						.findViewById(R.id.searchInvalidTVID);
				searchAlertTV.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog2.findViewById(R.id.inValidOKBtnID);
				okbutton.setTypeface(Utility.font_bold);

				alertDialog2.setCancelable(false);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
					}
				});
			}
		
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		if (response != null) {
			if (response instanceof String) {
				Utility.showMessage(this, response.toString());
			} else {
				try {
					if (response != null && eventType == 4) {
						// categoryList = (ArrayList<Category>)response;
						categoryList.clear();
						categoryListTemp.clear();
						categoryList.addAll((Collection<Category>) response);
						if (categoryListTemp != null) {
							int position = categoryListTemp.size();
							categoryListTemp.addAll(categoryList);
							categoryList = categoryListTemp;
							searchListView.setSelection(position);
						}
						mAdapter = new SearchListAdapter(this);
						searchListView.setAdapter(mAdapter);
					}
					loading.setVisibility(View.GONE);
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	protected void onRestart() {
		menuListView.setVisibility(ListView.GONE);
		super.onRestart();
	}

	@Override
	public boolean onLongClick(View v) {
		boolean returnValue = true;
		try {
			if (v.getId() == R.id.enterLocationETID) {
				if (locationET.getText().length() > 25) {
					returnValue = true;

				} else {
					returnValue = false;
				}
			} else if (v.getId() == R.id.enterKeywordETID) {
				if (keywordET.getText().length() > 25) {
					returnValue = true;

				} else {
					returnValue = false;
				}
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
		return returnValue;
	}
}
