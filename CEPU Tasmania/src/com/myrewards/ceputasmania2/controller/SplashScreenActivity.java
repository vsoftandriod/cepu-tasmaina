package com.myrewards.ceputasmania2.controller;




import org.jsoup.Jsoup;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.TextView;

import com.myrewards.ceputasmania2.utils.DatabaseHelper;
import com.myrewards.ceputasmania2.utils.Utility;

@SuppressLint("HandlerLeak") public class SplashScreenActivity<TextProgressBar> extends Activity implements AnimationListener {

	// stopping splash screen starting home activity.
	private static final int STOPSPLASH = 0;
	// time duration in millisecond for which your splash screen should visible
	// to
	// user. here i have taken half second
	private static final long SPLASHTIME = 2000;	
	int currentAPILevel;
	// handler for splash screen
	
	DatabaseHelper helper=new DatabaseHelper(this);
	
	private Handler splashHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case STOPSPLASH:
				boolean newVersion = false;
				if (Utility.isOnline((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE))) {
					if (!(currentAPILevel > 8)) {
						Intent intent = new Intent(SplashScreenActivity.this, LoginScreenActivity.class);
						startActivity(intent);
						SplashScreenActivity.this.finish();
					}
					else {
						try {
							newVersion=web_update();
						} catch (Exception e) {
							Intent intent = new Intent(SplashScreenActivity.this, LoginScreenActivity.class);
							startActivity(intent);
							SplashScreenActivity.this.finish();
						} catch (NoSuchMethodError e) {
							Intent intent = new Intent(SplashScreenActivity.this, LoginScreenActivity.class);
							startActivity(intent);
							SplashScreenActivity.this.finish();
						}
						
						if (newVersion == true) {
							showDialog(1);
						} else {
							Intent intent = new Intent(SplashScreenActivity.this, LoginScreenActivity.class);
							startActivity(intent);
							SplashScreenActivity.this.finish();
						}
					}
				}
				else {
					showDialog(2);
				}
				break;
			}
			super.handleMessage(msg);
		}
		
		private boolean web_update() {
			try {
				String package_name = getPackageName();
				String curVersion = getApplicationContext().getPackageManager()
						.getPackageInfo(package_name, 0).versionName;
				String newVersion = curVersion;
				newVersion = Jsoup
						.connect(
								"https://play.google.com/store/apps/details?id="
										+ package_name + "&hl=en")
						.timeout(30000)
						.userAgent(
								"Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
						.referrer("http://www.google.com").get()
						.select("div[itemprop=softwareVersion]").first()
						.ownText();
				return (value(curVersion) < value(newVersion)) ? true : false;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}

		private long value(String string) {
			string = string.trim();
			if (string.contains(".")) {
				final int index = string.lastIndexOf(".");
				return value(string.substring(0, index)) * 100
						+ value(string.substring(index + 1));
			} else {
				return Long.valueOf(string);
			}
		}	
	};
	//dialogs
		@Override
		protected Dialog onCreateDialog(int id) {
			try {
				if (id == 1) {
					AlertDialog callMobiledialog = null;
					switch (id) {
					case 1:
						LayoutInflater liYes = LayoutInflater.from(this);
						View callAddressView = liYes.inflate(
								R.layout.dialog_layout_update_version, null);
						AlertDialog.Builder adbrok = new AlertDialog.Builder(this);
						adbrok.setView(callAddressView);
						callMobiledialog = adbrok.create();
						break;
					}
					return callMobiledialog;
				}
				else if (id == 2) {
					AlertDialog noNetworkDialog = null;
					LayoutInflater noNetInflater = LayoutInflater.from(this);
					View noNetworkView = noNetInflater.inflate(
							R.layout.dialog_layout_no_network, null);
					AlertDialog.Builder adbNoNet = new AlertDialog.Builder(this);
					adbNoNet.setCancelable(false);
					adbNoNet.setView(noNetworkView);
					noNetworkDialog = adbNoNet.create();
					return noNetworkDialog;
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("HARI-->Debug", e);
					e.printStackTrace();
				}
			}
			return super.onCreateDialog(id);
		}

		@Override
		protected void onPrepareDialog(int id, Dialog dialog) {
			try {
				switch (id) {
				case 1:
					final AlertDialog alt3 = (AlertDialog) dialog;

					TextView alertTitle = (TextView) alt3
							.findViewById(R.id.favGINTitleTVID);
					alertTitle.setTypeface(Utility.font_bold);

					TextView tv22 = (TextView) alt3.findViewById(R.id.addFavTVID);
					tv22.setTypeface(Utility.font_reg);
					tv22.setText(getResources().getString(
							R.string.update_version_message));
					Button addFavYesBtn = (Button) alt3
							.findViewById(R.id.add_fav_yesBtnID);
					addFavYesBtn.setTypeface(Utility.font_bold);
					Button addNoFavBtn = (Button) alt3
							.findViewById(R.id.add_fav_noBtnID);
					addNoFavBtn.setTypeface(Utility.font_bold);
					addFavYesBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {try{
							if(helper.getLoginDetails()!=null)
							helper.deleteLoginDetails();
							}
							catch(Exception e)
							{
								e.printStackTrace();
							}
						startActivity(new Intent(SplashScreenActivity.this,	AppPushNotificationActivity.class));
						SplashScreenActivity.this.finish();
						alt3.dismiss();}
					});
					addNoFavBtn.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {

							alt3.dismiss();
							Intent intent = new Intent(SplashScreenActivity.this, LoginScreenActivity.class);
							startActivity(intent);
							SplashScreenActivity.this.finish();
						}
					});
					break;
				case 2:
					final AlertDialog alertDialog2 = (AlertDialog) dialog;
					TextView textTv = (TextView) alertDialog2.findViewById(R.id.noConnTVID);
					
					textTv.setTypeface(Utility.font_reg);
					
					TextView alertTitle2 = (TextView) alertDialog2
							.findViewById(R.id.alertLogoutTitleTVID);
					
					alertTitle2.setTypeface(Utility.font_bold);
					
					Button okbutton = (Button) alertDialog2
							.findViewById(R.id.noNetWorkOKID);
					okbutton.setTypeface(Utility.font_bold);
					alertDialog2.setCancelable(false);
					okbutton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							SplashScreenActivity.this.finish();
							alertDialog2.dismiss();
						}
					});
				}

				super.onPrepareDialog(id, dialog);
			} catch (Exception e) {
				if (e != null) {
					Log.w("HARI-->Debug", e);
					e.printStackTrace();
				}
			}
		}
	
	@SuppressLint("HandlerLeak") @Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		try {
			setContentView(R.layout.splashscreen); 
		} catch (InflateException e) {
			setContentView(R.layout.splashscreen); 
		}
		currentAPILevel = Integer.valueOf(android.os.Build.VERSION.SDK);
		
		try
		{
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy); 
		}
		catch(Exception e)
		{
			if(e!=null)
			{
				e.printStackTrace();
			}
		}
		
		
		Message msg = new Message();
		msg.what = STOPSPLASH;
		splashHandler.sendMessageDelayed(msg, SPLASHTIME);
	}

	@Override
	public void onAnimationEnd(Animation arg0) {
		System.out.println("This is under animation starts ");
	}

	@Override
	public void onAnimationRepeat(Animation arg0) {

	}

	@Override
	public void onAnimationStart(Animation arg0) {
	}
}