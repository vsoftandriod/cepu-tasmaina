package com.myrewards.ceputasmania2.model;


public class Category {
	private int catID;
	private int parent_id;
	private String name;
	private String description;
	private String details;
	private String sort;
	private String deleted;
	private String created;
	private String modified;
	private String countries;
	private String logo_extension;
	private String search_weight;
	private boolean isSelected;
	public int getCatID() {
		return catID;
	}
	public void setCatID(int catID) {
		this.catID = catID;
	}
	public int getParent_id() {
		return parent_id;
	}
	public void setParent_id(int parent_id) {
		this.parent_id = parent_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public String getCountries() {
		return countries;
	}
	public void setCountries(String countries) {
		this.countries = countries;
	}
	public String getLogo_extension() {
		return logo_extension;
	}
	public void setLogo_extension(String logo_extension) {
		this.logo_extension = logo_extension;
	}
	public String getSearch_weight() {
		return search_weight;
	}
	public void setSearch_weight(String search_weight) {
		this.search_weight = search_weight;
	}
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	
	

}
