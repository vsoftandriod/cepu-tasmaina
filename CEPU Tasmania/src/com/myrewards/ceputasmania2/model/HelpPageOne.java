package com.myrewards.ceputasmania2.model;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myrewards.ceputasmania2.controller.R;
import com.myrewards.ceputasmania2.utils.Utility;


/**
 * @author HARI
 *
 */
public class HelpPageOne extends Fragment {
	

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v=(LinearLayout)inflater.inflate(R.layout.page1_frag1_layout, container, false);
		TextView textview1=(TextView)v.findViewById(R.id.page1_IVID);
		textview1.setMovementMethod(new ScrollingMovementMethod());
		textview1.setTypeface(Utility.font_reg);
		textview1.setText(Html.fromHtml("<h4>  Welcome to your Rewards Program... </h4> Enjoy ongoing, uninterrupted acess to thousands of discounts on your phone. <br><br>&#9733; You can search by category, by keyword and also search local with \"What around me\".<br><br>&#9733; Select a merchant offer you like and present the coupon on your phone at the time of settling your bill. Always check the terms of the offer.<br><br>&#9733; Some merchants only offer their services online and in most cases should be able to click through to their online offerings.<br><br><br><br><br><br><br>"));
		return v;
	}
}
