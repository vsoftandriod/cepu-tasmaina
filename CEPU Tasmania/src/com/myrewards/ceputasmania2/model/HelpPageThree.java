package com.myrewards.ceputasmania2.model;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myrewards.ceputasmania2.controller.R;
import com.myrewards.ceputasmania2.utils.Utility;

/**
 * @author HARI
 *
 */
public class HelpPageThree extends Fragment {

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) 
	{
		View v=(LinearLayout)inflater.inflate(R.layout.page3_frag3_layout, container, false);
		TextView textview1=(TextView)v.findViewById(R.id.page1_IVID);
		textview1.setTypeface(Utility.font_reg);
		textview1.setMovementMethod(new ScrollingMovementMethod());
		textview1.setText(Html.fromHtml("<h4>  What's Around Me! </h4> This local search function show offers that are around you wherever you are. <br><br>&#9733; You can expand or reduce the search area on screen.<br><br>&#9733;  The offer drop pins have an icon representing type of offer.<br><br>&#9733; For example dining has a knife and fork icon pin. <br><br>&#9733; The icon range are shown in the main search function. <br><br><br><br><br><br><br><br><br>"));
		return v;	
	}

	

}
