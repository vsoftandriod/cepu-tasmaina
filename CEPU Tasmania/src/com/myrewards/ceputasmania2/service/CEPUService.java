package com.myrewards.ceputasmania2.service;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.myrewards.ceputasmania2.model.ProductNotice;
import com.myrewards.ceputasmania2.model.User;
import com.myrewards.ceputasmania2.utils.ApplicationConstants;
import com.myrewards.ceputasmania2.utils.Utility;
import com.myrewards.ceputasmania2.xml.NoticeBoardParser;
import com.myrewards.ceputasmania2.xml.UserDetailsParser;

public class CEPUService implements CEPUNetworkListener {
	private static final int REQUEST_LOGIN = 1;
	private static final int REQUEST_USER_DETAILS = 2;
	// private static final int REQUEST_CLIENT_BANNER = 3;
	private static final int REQUEST_CATEGORY_LIST = 4;
	private static final int REQUEST_HOT_OFFERS = 5;
	private static final int REQUEST_SEARCH_PRODUCTS = 6;
	private static final int REQUEST_PRODUCTS_DETAILS = 7;
	private static final int REQUEST_TEST = 8;
	private static final int REQUEST_MY_ACCOUNT = 9;
	private static final int REQUEST_MY_NOTICE = 10;

	private CEPUServiceListener cEPUServiceListener;
	private static CEPUService myRewardsService;
	private String username;

	public static CEPUService getCEPUService() {
		if (myRewardsService == null) {
			myRewardsService = new CEPUService();
		}
		return myRewardsService;
	}

	public void sendTestRequestAbout(
			CEPUServiceListener cEPUServiceListener) {
		this.cEPUServiceListener = cEPUServiceListener;
		CEPUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.ABOUT_US_WRAPPER, null, null, this,
				REQUEST_TEST);
	}

	public void sendTestRequestDeals(
			CEPUServiceListener cEPUServiceListener) {
		this.cEPUServiceListener = cEPUServiceListener;
		CEPUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.MY_DEAL_WRAPPER, null, null, this,
				REQUEST_TEST);
	}

	public void sendLoginRequest(
			CEPUServiceListener cEPUServiceListener,
			String username, String password, String affiliateId) {
		this.cEPUServiceListener = cEPUServiceListener;
		this.username = username;
		CEPUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.LOGIN_WRAPPER,
				getLoginRequestData(username, password, affiliateId), null,
				this, REQUEST_LOGIN);
	}

	private String getLoginRequestData(String username, String password,
			String subDomainURL) {
		String loginXML = "uname=" + username + "&pwd=" + password + "&sub="
				+ subDomainURL;
		return loginXML;
	}

	private void sendUserDetailsRequest() {
		CEPUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.USER_DETAILS_WRAPPER + username, null,
				null, this, REQUEST_USER_DETAILS);
	}

	public void sendCategoriesListRequest(
			CEPUServiceListener cEPUServiceListener) {
		this.cEPUServiceListener = cEPUServiceListener;
		CEPUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.CATEGORY_WRAPPER + "&cid="
						+ Utility.user.getClient_id() + "&country="
						+ Utility.user.getCountry(), null, null, this,
				REQUEST_CATEGORY_LIST);
	}

	public void sendHotOffersRequest(
			CEPUServiceListener cEPUServiceListener) {
		this.cEPUServiceListener = cEPUServiceListener;
		CEPUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.HOT_OFFERS_WRAPPER
						+ Utility.user.getClient_id(), null, null, this,
				REQUEST_HOT_OFFERS);
	}

	public void sendSearchProductsRequest(
			CEPUServiceListener cEPUServiceListener,
			String catID, String location, String keyword) {
		this.cEPUServiceListener = cEPUServiceListener;
		String queryString = "cid=" + Utility.user.getClient_id();
		if (catID != null) {
			queryString = queryString + "&cat_id=" + catID;
		}
		if (location.length() > 0) {
			queryString = queryString + "&p=" + location;
		}
		if (keyword.length() > 0) {
			queryString = queryString + "&q=" + keyword;
		}
		CEPUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.SEARCH_PRODUCTS_WRAPPER + queryString
						+ "&country=Australia&start=0&limit=30", null, null,
				this, REQUEST_SEARCH_PRODUCTS);
	}

	public void sendProductDetailsRequest(
			CEPUServiceListener cEPUServiceListener,
			int productId) {
		this.cEPUServiceListener = cEPUServiceListener;
		CEPUHttpClient.getWWDispatchHandler()
				.sendRequestAsync(
						ApplicationConstants.PRODUCT_DETAILS_WRAPPER + "id="
								+ productId, null, null, this,
						REQUEST_PRODUCTS_DETAILS);
	}

	public void sendMyAccountRequest(
			CEPUServiceListener cEPUServiceListener,
			int username, String firstName, String lastName,String email,String country,String state,
			String newsLetter) {
		this.cEPUServiceListener = cEPUServiceListener;
		CEPUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.MY_ACCOUNT_WRAPPER,
				getMyAccountRequestData(username, firstName, lastName,email,country,state,
						newsLetter), null, this, REQUEST_MY_ACCOUNT);
	}

	private String getMyAccountRequestData(int username, String firstName,
			String lastName,String email, String country, String state,String newsLetter) {
		String requestData = "uname=" + username + "&fname=" + firstName
				+ "&lname="+ lastName+"&email="+email+"&country=" + country +"&state="+state + "&newsletter="
				+ newsLetter;
		return requestData;
	}


	public void sendMyNoticeBoardRequest(
			CEPUServiceListener cEPUServiceListener) {
		this.cEPUServiceListener = cEPUServiceListener;
		CEPUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.NOTICE_BOARD_WRAPPER_DAILY_SAVERS, null, null, this,
				REQUEST_MY_NOTICE);
	}
	

	public void onRequestCompleted(String response, String errorString,
			int eventType) {
		switch (eventType) {
		case REQUEST_LOGIN:
			if (errorString == null && response != null) {
				try {
					DocumentBuilderFactory factory = DocumentBuilderFactory
							.newInstance();
					DocumentBuilder db = factory.newDocumentBuilder();
					InputSource inStream = new InputSource();
					inStream.setCharacterStream(new StringReader(response));
					Document doc = db.parse(inStream);

					String message = "status";
					NodeList messageId_nl = doc.getElementsByTagName("status");
					for (int i = 0; i < messageId_nl.getLength(); i++) {
						if (messageId_nl.item(i).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
							org.w3c.dom.Element nameElement = (org.w3c.dom.Element) messageId_nl
									.item(i);
							message = nameElement.getFirstChild()
									.getNodeValue().trim();
						}
					}
					if (message != null && message.equalsIgnoreCase("SUCCESS")) {
						sendUserDetailsRequest();
					} else
						cEPUServiceListener.onServiceComplete(
								message, eventType);

				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (errorString != null) {
				cEPUServiceListener.onServiceComplete(errorString,
						eventType);
			} else {
			}
			break;
		case REQUEST_USER_DETAILS:
			if (errorString == null && response != null) {
				try {
					User user = new User();
					new UserDetailsParser().internalXMLParse(response, user);
					cEPUServiceListener.onServiceComplete(user,
							eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cEPUServiceListener
							.onServiceComplete(ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cEPUServiceListener.onServiceComplete(errorString,
						eventType);
			} else {
			}
			break;
		case REQUEST_CATEGORY_LIST:
			break;
		case REQUEST_HOT_OFFERS:
			break;
		case REQUEST_SEARCH_PRODUCTS:
			break;
		case REQUEST_PRODUCTS_DETAILS:
			break;
		case REQUEST_MY_ACCOUNT:
			if (errorString == null && response != null) {
				try {
					cEPUServiceListener.onServiceComplete(response,
							eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cEPUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cEPUServiceListener.onServiceComplete(errorString,
						eventType);
			} else {
			}
			break;
		case REQUEST_MY_NOTICE:
			if (errorString == null && response != null) {
				try {
					List<ProductNotice> noticeBoardList = new ArrayList<ProductNotice>();
					new NoticeBoardParser().internalXMLParse(response,
							noticeBoardList);
					cEPUServiceListener.onServiceComplete(
							noticeBoardList, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cEPUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cEPUServiceListener.onServiceComplete(errorString,
						eventType);
			} else {
			}
			break;
		case REQUEST_TEST:
			if (errorString == null && response != null) {
				try {

					cEPUServiceListener.onServiceComplete(response,
							eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cEPUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cEPUServiceListener.onServiceComplete(errorString,
						eventType);
			} else {
			}
			break;
		}
	}

	
}
