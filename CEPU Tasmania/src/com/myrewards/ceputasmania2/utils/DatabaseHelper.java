package com.myrewards.ceputasmania2.utils;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.myrewards.ceputasmania2.model.Contact;
import com.myrewards.ceputasmania2.model.LoginDetails;
import com.myrewards.ceputasmania2.model.ParkingDetails;
import com.myrewards.ceputasmania2.model.Product;
import com.myrewards.ceputasmania2.model.User;

public class DatabaseHelper extends SQLiteOpenHelper {
	static final String dbName = "GrabItNow";
	static final String loginTable = "loginNames";
	static final String favoriteTable = "product";
	static final String userTable = "userDetails";
	static final String contactsTable = "contactsDetails";
	static final String parkingTimerTable = "parkingTimerTable";
	static final String name = "Username";
	static final String pwd = "Password";
	static final String subDomain = "subDomain";

	// Notice Board tables.......
	static final String noticeboardid = "noticeboardiddetails";
	static final String noticeId = "noticeid";
	static final String noticerealId = "noticerealid";
	static final String statusId = "status";

	// user table columns starts
	static final String id = "id";// int
	static final String client_id = "client_id";// int
	static final String domain_id = "domain_id";// int
	static final String type = "type";
	static final String username = "username";
	static final String email = "email";
	static final String first_name = "first_name";
	static final String last_name = "last_name";
	static final String state = "state";
	static final String country = "country";
	static final String mobile = "mobile";// int
	static final String card_ext = "card_ext";
	static final String client_name = "client_name";
	static final String newsletter = "newsletter";
	static final String passwod = "password";
	// user table columns ends

	// contacts table columns
	static final String contactName = "contactName";
	static final String contactNumber = "contactNumber";

	// parking timer table columns
	static final String startTime = "startTime";
	static final String endTime = "endTime";
	static final String lattitude = "lattitude";
	static final String longittude = "longittude";
	static final String parkingTimerId = "id";

	// static final String cookie = "cookie";
	static final String date = "loginDate";
	static final String productId = "productId";
	static final String productName = "productName";
	static final String productHighlight = "productHighlight";

	// this table is for my email send
	static final String myEmailSendTable = "MyEmailSend";

	static final String Ename = "ename";
	static final String Eemail = "eemail";

	//this table is for notices update on or off
	
		static final String noticeUpdateStateTable="noticeupdatestatetable";
		static final String noticeUpdateState="noticeupdatestate";
	
	public DatabaseHelper(Context context) {
		super(context, dbName, null, 39);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		/*
		 * db.execSQL("CREATE TABLE "+loginTable+" ("+name+ " TEXT, "+ pwd+
		 * " TEXT, "+ subDomain+" TEXT, "+ cookie+" TEXT, "+ date+" TEXT)");
		 */
		db.execSQL("CREATE TABLE " + loginTable + " (" + name + " TEXT, " + pwd
				+ " TEXT, " + subDomain + " TEXT, " + date + " TEXT)");
		db.execSQL("CREATE TABLE " + favoriteTable + " (" + productId
				+ " TEXT, " + productName + " TEXT, " + productHighlight
				+ " TEXT)");
		db.execSQL("CREATE TABLE " + userTable + " (" + id + " INTEGER, "
				+ client_id + " INTEGER, " + domain_id + " INTEGER, " + type
				+ " TEXT, " + username + " TEXT, " + email + " TEXT, "
				+ first_name + " TEXT, " + last_name + " TEXT, " + state
				+ " TEXT, " + country + " TEXT, " + mobile + " TEXT, "
				+ card_ext + " TEXT, " + client_name + " TEXT, " + newsletter
				+ " TEXT, " + passwod + " TEXT)");
		db.execSQL("CREATE TABLE " + contactsTable + " (" + contactName
				+ " TEXT, " + contactNumber + " TEXT PRIMARY KEY NOT NULL)");
		db.execSQL("CREATE TABLE " + parkingTimerTable + " (" + startTime
				+ " TEXT, " + endTime + " TEXT, " + lattitude + " TEXT, "
				+ parkingTimerId + " TEXT, " + longittude + " TEXT)");
		db.execSQL("CREATE TABLE " + noticeboardid + " (" + noticeId
				+ " TEXT, " + noticerealId + " TEXT, " + statusId + " TEXT)");
		db.execSQL("CREATE TABLE " + myEmailSendTable + " (" + Ename + " TEXT,"
				+ Eemail + " TEXT)");
		
		db.execSQL("CREATE TABLE "+noticeUpdateStateTable+"("+noticeUpdateState+" TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + loginTable);
		db.execSQL("DROP TABLE IF EXISTS " + favoriteTable);
		db.execSQL("DROP TABLE IF EXISTS " + userTable);
		db.execSQL("DROP TABLE IF EXISTS " + contactsTable);
		db.execSQL("DROP TABLE IF EXISTS " + parkingTimerTable);
		db.execSQL("DROP TABLE IF EXISTS " + noticeboardid);
		db.execSQL("DROP TABLE IF EXISTS " + noticeUpdateStateTable);
		db.execSQL("DROP TABLE IF EXISTS " + myEmailSendTable);
		onCreate(db);
	}

	public void addLoginDetails(String username, String password,
			String subDomain1, String loginDate) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + loginTable);
		ContentValues cv = new ContentValues();
try{
		cv.put(name, username);
		cv.put(pwd, password);
		cv.put(subDomain, subDomain1);
		cv.put(date, loginDate);

		db.insert(loginTable, name, cv);
		db.close();
}catch(Exception e){}
	}

	public void deleteLoginDetails() {
		// TODO Auto-generated method stub
		try{
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + loginTable);
		}catch(Exception e){}
	}
	
	public void addFavoriteProductDetails(String id, String name, String highlight) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
try{
		cv.put(productId, id);
		cv.put(productName, name);
		cv.put(productHighlight, highlight);

		db.insert(favoriteTable, productId, cv);
		db.close();
}catch(Exception e){}
	}

	public void addUserDetails(User user) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + userTable);
		ContentValues cv = new ContentValues();
try{
		cv.put(id, user.getId());
		cv.put(client_id, user.getClient_id());
		cv.put(domain_id, user.getDomain_id());
		cv.put(type, user.getType());
		cv.put(username, user.getUsername());
		cv.put(email, user.getEmail());
		cv.put(first_name, user.getFirst_name());
		cv.put(last_name, user.getLast_name());
		cv.put(state, user.getState());
		cv.put(country, user.getCountry());
		cv.put(mobile, user.getMobile());
		cv.put(card_ext, user.getCard_ext());
		cv.put(client_name, user.getClient_name());
		cv.put(newsletter, user.getNewsletter());
		cv.put(passwod, user.getPasswod());

		db.insert(userTable, name, cv);
		db.close();
}catch(Exception e){}
	}

	public void addContact(Contact contact) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		try{
		cv.put(contactName, contact.getName());
		cv.put(contactNumber, contact.getNumber());
		db.insert(contactsTable, contactNumber, cv);
		}catch(Exception e){}
		db.close();
	}

	public void addParkingtimerDetails(ParkingDetails parkingDetails) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + parkingTimerTable);
		ContentValues cv = new ContentValues();

		cv.put(startTime, parkingDetails.getStartTime());
		cv.put(endTime, parkingDetails.getEndTime());
		cv.put(lattitude, parkingDetails.getLattitude());
		cv.put(longittude, parkingDetails.getLongittude());
		cv.put(parkingTimerId, parkingDetails.getId());
		db.insert(parkingTimerTable, parkingTimerId, cv);
		db.close();
	}

	public LoginDetails getLoginDetails() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db
				.rawQuery("SELECT * FROM " + loginTable, new String[] {});
		LoginDetails loginDetails = null;
		try{
		if (cur.moveToFirst()) {
			do {
				loginDetails = new LoginDetails();
				loginDetails
						.setUsername(cur.getString(cur.getColumnIndex(name)));
				loginDetails
						.setPassword(cur.getString(cur.getColumnIndex(pwd)));
				loginDetails.setSubDomain(cur.getString(cur
						.getColumnIndex(subDomain)));
				// loginDetails.setCookie(cur.getString(cur.getColumnIndex(cookie)));
				loginDetails.setLoginDate(cur.getString(cur
						.getColumnIndex(date)));
			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		}catch(Exception e){}
		if (loginDetails != null) {
			return loginDetails;
		} else {
			return null;
		}

	}

	@SuppressWarnings("unused")
	public List<Product> getProductList() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT * FROM " + favoriteTable,
				new String[] {});
		int i = 0;
		List<Product> productsList = new ArrayList<Product>();
		try{
		if (cur.moveToFirst()) {
			do {
				Product product = new Product();
				String id = cur.getString(cur.getColumnIndex(productId));
				product.setId(Integer.parseInt(id));
				product.setName(cur.getString(cur.getColumnIndex(productName)));
				product.setHighlight(cur.getString(cur
						.getColumnIndex(productHighlight)));
				productsList.add(product);
				i++;
			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		}catch(Exception e){}
		return productsList;
	}

	public boolean isProductExist(String productId1) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT " + productId + " FROM "
				+ favoriteTable, new String[] {});
		String productIdStored = null;
		try{
		if (cur.moveToFirst()) {
			do {
				productIdStored = cur.getString(cur.getColumnIndex(productId));
				if (productIdStored.equals(productId1)) {
					return true;
				}
			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		}catch(Exception e){}
		return false;
	}

	public String getPasswd(String uName) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db
				.rawQuery("SELECT * FROM " + loginTable, new String[] {});
		String passwd = null;
		try{
		if (cur.moveToFirst()) {
			String username = null;
			do {
				username = cur.getString(cur.getColumnIndex(name));
				if (username.equals(uName)) {
					passwd = cur.getString(cur.getColumnIndex(pwd));
				}
			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		}catch(Exception e){}
		return passwd;
	}

	public String getUrl() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT * FROM " + favoriteTable, null);
		String urlString = null;
		try{
		if (cur.moveToFirst()) {

			do {
				urlString = cur.getString(cur.getColumnIndex(subDomain));

			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		}catch(Exception e){}
		return urlString;
	}

	public void updatePasswd(String userId, String passwd) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		try{
		cv.put(name, userId);
		cv.put(pwd, passwd);
		db.update(loginTable, cv, name + "='" + userId + "'", null);
		db.close();
		}catch(Exception e){}
	}

	public void clearPassword(String userName) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.close();
	}

	public void deleteProduct(String productId2) {
		SQLiteDatabase db = this.getWritableDatabase();
		try{
		db.execSQL("DELETE FROM " + favoriteTable + " WHERE " + productId + "="
				+ productId2);
		db.close();
		}catch(Exception e){}
	}

	public void deleteContact(String contactNo) {
		SQLiteDatabase db = this.getWritableDatabase();
		try
		{
		db.execSQL("DELETE FROM " + contactsTable + " WHERE " + contactNumber
				+ "=" + contactNo);
		db.close();
		}catch(Exception e){}
	}

	public void deleteParkingTimerDetails(String parkingTimerid) {
		SQLiteDatabase db = this.getWritableDatabase();
		try{
		db.execSQL("DELETE FROM " + parkingTimerTable + " WHERE "
				+ parkingTimerId + "=" + parkingTimerid);
		db.close();
		}catch(Exception e){}
	}

	@SuppressWarnings("unused")
	public ArrayList<Contact> getContactsList() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT * FROM " + contactsTable,
				new String[] {});
		int i = 0;
		List<Contact> conList = new ArrayList<Contact>();
		try{
		if (cur.moveToFirst()) {
			do {
				Contact contact = new Contact();
				contact.setName(cur.getString(cur.getColumnIndex(contactName)));
				contact.setNumber(cur.getString(cur
						.getColumnIndex(contactNumber)));
				conList.add(contact);
				i++;
			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
		}catch(Exception e){}
		return (ArrayList<Contact>) conList;
	}

	public User getUserDetails() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT * FROM " + userTable, new String[] {});
		User user = null;
try{
		if (cur.moveToFirst()) {
			do {
				user = new User();
				user.setId(cur.getInt(cur.getColumnIndex(id)));
				user.setClient_id(cur.getInt(cur.getColumnIndex(client_id)));
				user.setDomain_id(cur.getInt(cur.getColumnIndex(domain_id)));
				user.setType(cur.getString(cur.getColumnIndex(type)));
				user.setUsername(cur.getString(cur.getColumnIndex(username)));
				user.setEmail(cur.getString(cur.getColumnIndex(email)));
				user.setFirst_name(cur.getString(cur.getColumnIndex(first_name)));
				user.setLast_name(cur.getString(cur.getColumnIndex(last_name)));
				user.setState(cur.getString(cur.getColumnIndex(state)));
				user.setCountry(cur.getString(cur.getColumnIndex(country)));
				user.setMobile(cur.getString(cur.getColumnIndex(mobile)));
				user.setCard_ext(cur.getString(cur.getColumnIndex(card_ext)));
				user.setClient_name(cur.getString(cur
						.getColumnIndex(client_name)));
				user.setNewsletter(cur.getString(cur.getColumnIndex(newsletter)));
				user.setPasswod(cur.getString(cur.getColumnIndex(passwod)));

			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
}catch(Exception e){}
		if (user != null) {
			return user;
		} else {
			return null;
		}
	}

	public ParkingDetails getParkingTimerDetails() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT * FROM " + parkingTimerTable,
				new String[] {});
		ParkingDetails parkingDetails = null;
try{
		if (cur.moveToFirst()) {
			do {
				parkingDetails = new ParkingDetails();
				parkingDetails.setStartTime(cur.getString(cur
						.getColumnIndex(startTime)));
				parkingDetails.setEndTime(cur.getString(cur
						.getColumnIndex(endTime)));
				parkingDetails.setLattitude(cur.getString(cur
						.getColumnIndex(lattitude)));
				parkingDetails.setLongittude(cur.getString(cur
						.getColumnIndex(longittude)));
				parkingDetails.setId(cur.getString(cur
						.getColumnIndex(parkingTimerId)));
			} while (cur.moveToNext());
		}
		cur.close();
		db.close();
}catch(Exception e){}
		if (parkingDetails != null) {
			return parkingDetails;
		} else {
			return null;
		}
	}

	// Notice Board Added details

		public void addnoticeiddetails(String noticeids) {
			SQLiteDatabase db = this.getWritableDatabase();
			// db.execSQL("DELETE FROM "+noticeboardid);
			try{
			ContentValues cv = new ContentValues();
			// for(int i=0;i<NoticeboardIdParser.noticeboard_id_count;i++)
			// {
			// if(i==j)
			// {
		
			cv.put("noticerealId", noticeids);
			cv.put(statusId, "OPEN");
			db.insert(noticeboardid, null, cv);
			db.close();
			}catch(Exception e){}
			// }
			// }
		}

		public void updateNoticeDetails(String p) {
			SQLiteDatabase db = this.getWritableDatabase();
			ContentValues cv = new ContentValues();
			try{
			cv.put(statusId, "CLOSE");
			db.update(noticeboardid, cv, noticerealId + "='" + p + "'", null);
			db.close();
			}catch(Exception e){}
		}
		public void deaActivateNoticeDetails(String p) {
			SQLiteDatabase db = this.getWritableDatabase();
			ContentValues cv = new ContentValues();
			try{
			cv.put(statusId, "DEACTIVATED");
			db.update(noticeboardid, cv, noticerealId + "='" + p + "'", null);
			db.close();
			}catch(Exception e){}
		}
		public int getNoticeDetails() {
			SQLiteDatabase db = this.getReadableDatabase();
			int a=0;
			try{
			Cursor cur = db.rawQuery("SELECT * FROM " + noticeboardid + "  WHERE "
					+ statusId + " = " + "'OPEN'", null);

			// Cursor cur = db.rawQuery("SELECT * FROM "+noticeboardid+" WHERE "+
			// statusId +"="+"OPEN", new String [] {});
			 a = cur.getCount();
			cur.close();
			db.close();
			}catch(Exception e){}
			return a;
		}
		
		public List<String> getExistingNoticeRealIDs() {
			SQLiteDatabase db = this.getWritableDatabase();
			List<String> list = new ArrayList<String>();
			try{
			Cursor c = db.rawQuery("SELECT * FROM " + noticeboardid, null);
			if (c.moveToFirst()) {
				do {
					list.add(c.getString(1));
				} while (c.moveToNext());
			}
			if (c != null && !c.isClosed()) {
				c.close();
			}
			c.close();
			db.close();
			}catch(Exception e){}
			return list;
		}
		
		public void deleteNoticeIdDetails(String noticeId) {
			SQLiteDatabase db = this.getWritableDatabase();
			try{
			db.execSQL("DELETE FROM " + noticeboardid + " WHERE " + noticerealId + "="
					+ noticeId);
			db.close();
			}catch(Exception e){}
		}
		public String getNoticeIdReadorUnread(String pos) {
			String b = "CLOSE";
			SQLiteDatabase db = this.getReadableDatabase();
			try{
			Cursor cur = db.rawQuery("SELECT " + statusId + " FROM "
					+ noticeboardid + "  WHERE " + noticerealId + " = " + pos,
					new String[] {});
			if (cur.moveToFirst()) {
				if (cur.getString(0).equals("CLOSE")) {
					b = "CLOSE";
				} else if(cur.getString(0).equals("OPEN")) {
					b = "OPEN";
				}
				
			}
			cur.close();
			db.close();
			}catch(Exception e){}
			return b;
		}

		public List<String> getExistingIDs() {
			SQLiteDatabase db = this.getWritableDatabase();
			List<String> list = new ArrayList<String>();
			try{
			Cursor c = db.rawQuery("SELECT * FROM " + noticeboardid, null);
			if (c.moveToFirst()) {
				do {
					list.add(c.getString(1));
				} while (c.moveToNext());
			}
			if (c != null && !c.isClosed()) {
				c.close();
			}
			c.close();
			db.close();
			}catch(Exception e){}
			return list;
		}
		
		//this is for notice updates
		
		public void setNoticeUpdateState(String state)
		{
			SQLiteDatabase db = this.getWritableDatabase();
			db.execSQL("DELETE FROM "+noticeUpdateStateTable);
			ContentValues cv = new ContentValues();
			try{
			cv.put(noticeUpdateState, state);
			db.insert(noticeUpdateStateTable, null, cv);
			db.close();
			}catch(Exception e){}
		}
		public String getNoticeUpdateState()
		{
			String status="NO_VALUE";
			SQLiteDatabase db = this.getReadableDatabase();
			Cursor cursor=db.rawQuery("SELECT "+noticeUpdateState+" FROM " + noticeUpdateStateTable, null);
			cursor.moveToFirst();
			try{
			if(cursor.getCount()>0)
			{
				status=cursor.getString(0);
				
			}
			cursor.close();
			db.close();
			}
			catch(Exception e)
			{
				
			}
			return status;
			
		
		}
}
