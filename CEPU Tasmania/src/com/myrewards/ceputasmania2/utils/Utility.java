package com.myrewards.ceputasmania2.utils;

import java.util.ArrayList;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.myrewards.ceputasmania2.controller.NoticeBoardReceiver;
import com.myrewards.ceputasmania2.model.Product;
import com.myrewards.ceputasmania2.model.User;

public class Utility {
	public static int screenHeight;
	public static int screenWidth;
	public static String COOKIE = null;
	public static User user = null;
	public static String DASHBOARD_ICON_ID = "DASHBOARD_ICON_ID";
	public static final int MY_UNION = 1;
	public static final int MY_DELEGATES = 2;
	public static final int MY_CARD = 3;
	public static final int MY_UNION_CONTACTS = 4;
	public static final int MY_NOTICE_BOARD = 5;
	public static final int SEND_A_FRIEND = 6;
	public static final int MY_SPECIAL_OFFERS = 7;
	public static final int SEARCH_MY_REWARDS = 8;
	public static final int MY_PARKING_TIME = 9;
	public static final int MY_ACCOUNT = 10;
	public static final int MY_ISSUE = 11;
	public static final int LAGOUT = 12;

	public static int fav_position_var;

	public static Double mLat;
	public static Double mLng;
	public static ArrayList<Product> productsListAroundMe = null;
	public static final String FETCH_DIRECTION_UP = "up";

	public static Typeface font_bold, font_reg;
	
	public static String userName=null;
	public static String pwd=null;
	
	//this is for notification update
	
	public static AlarmManager alarmMgr0=null;
	 public static PendingIntent pendingIntent0=null;
	
	public static void showMessage(Context ctx, String msg) {
		Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
	}

	public static boolean isOnline(ConnectivityManager cm) {
		/*
		 * ConnectivityManager cm = (ConnectivityManager)
		 * getSystemService(Context.CONNECTIVITY_SERVICE);
		 */
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	public static boolean copyPasteMethod(View v, int length) {

		boolean returnValue = false;
		if (length > 25) {
			returnValue = true;
		}

		return returnValue;

	}

	public static boolean copyPasteMethod2(View v, int length) {

		boolean returnValue = false;
		if (length > 40) {
			returnValue = true;
		}

		return returnValue;
	}

	public static void setNotificationReceiver(Context context) {
		 alarmMgr0 = (AlarmManager)context. getSystemService(Context.ALARM_SERVICE);

		// Create pending intent & register it to your alarm notifier class
		Intent intent0 = new Intent(context, NoticeBoardReceiver.class);
	
		  pendingIntent0 = PendingIntent.getBroadcast(context, 0,
				intent0, PendingIntent.FLAG_UPDATE_CURRENT);
		
		// set that timer as a RTC Wakeup to alarm manager object
		alarmMgr0.setRepeating(AlarmManager.ELAPSED_REALTIME,
				SystemClock.elapsedRealtime(), (3*60*60*1000), pendingIntent0);
		
	}
	public static void cancelNotificationReceiver()
	{
		if(alarmMgr0!=null && pendingIntent0!=null)
		alarmMgr0.cancel(pendingIntent0);
	}
	
	 /**
     * Obtains the LayoutInflater from the given context.
     */
    public static LayoutInflater from_HariContext(Context context) {
    	LayoutInflater layoutInflater = null;
    	try {
    		if (context != null) {
    			layoutInflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}
    		
    		if (layoutInflater == null) {
    			throw new AssertionError("LayoutInflater not found.");
    		}
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->DEBUG", e);
			}
			layoutInflater = null;
		}
        return layoutInflater;
    }
    
    /**
     * Obtains the ConnectivityManager from the given context.
     */
    public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			return false;
		} else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
